@extends('layouts.master')
@section('title', 'Search Payroll')
@section('customcss')
<style type="text/css">
    .mdi {
        line-height:7px;
        vertical-align: middle;
    }
</style>
@endsection
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-header">Payroll List</div>
                <div class="card-body table-responsive">
                    <div class="row">
                        <div class="col-md-8">
                            <a href="{{ url('/payrollledger') }}" class="btn btn-primary"><i class="mdi mdi-plus"></i>&nbsp;Generate Payroll</a>
                            <a href="{{ url('/bulk-payroll') }}" class="btn btn-primary"><i class="mdi mdi-plus"></i>&nbsp;Bulk Payroll</a>
                        </div>
                        @if(count($payrolls) > 1)
                        <div class="col-md-4">
                            <div class="pull-right">
                                <form action="?" method="GET" class="form-inline">
                                    @csrf 
                                    @if(isset($employee_id))
                                    <input type="hidden" name="employee_id" value="{{ $employee_id }}">
                                    @endif
                                    @if(isset($payroll_number))
                                    <input type="hidden" name="payroll_number" value="{{ $payroll_number }}">
                                    @endif
                                    @if(isset($department_id))
                                    <input type="hidden" name="department_id" value="{{ $department_id }}">
                                    @endif
                                    @if(isset($period_from))
                                    <input type="hidden" name="from" value="{{ date('Y-m-d', strtotime($period_from)) }}">
                                    @endif
                                    @if(isset($period_to))
                                    <input type="hidden" name="to" value="{{ date('Y-m-d', strtotime($period_to)) }}">
                                    @endif
                                    @if(isset($bill_number))
                                    <input type="hidden" name="bill_number" value="{{ $bill_number }}">
                                    @endif
                                    @if($payrolls->currentPage() !== NULL)
                                    <input type="hidden" name="page" value="{{ $payrolls->currentPage() }}">
                                    @endif
                                    <label>Show </label>
                                    <select class="form-control mx-2" name="perpage" onchange="this.form.submit()">
                                        <option value="10" {{app('request')->input('perpage') == 10 ? 'selected':''}}>10</option>
                                        <option value="25" {{app('request')->input('perpage') == 25 ? 'selected':''}}>25</option>
                                        <option value="50" {{app('request')->input('perpage') == 50 ? 'selected':''}}>50</option>
                                        <option value="100" {{app('request')->input('perpage') == 100 ? 'selected':''}}>100</option>
                                    </select>
                                    <label>entries</label>
                                </form>
                            </div>
                        </div>
                        @endif
                    </div>

                    <br>

                    <table class="table table-bordered table-striped" id="data_table">
                        <thead>
                            <tr>
                                <th class="text-center">Employee</th>
                                <th class="text-center">Batch #</th>
                                <th class="text-center">Payroll #</th>
                                <th class="text-center">From</th>
                                <th class="text-center">To</th>
                                <th class="text-center">Food</th>
                                <th class="text-center">Transportation</th>
                                <th class="text-center">SSS</th>
                                <th class="text-center">PhilHealth</th>
                                <th class="text-center">Pagibig</th>
                                <th class="text-center">Net Pay</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($fa = 0)
                            @php($ta = 0)
                            @php($ss = 0)
                            @php($ph = 0)
                            @php($pi = 0)
                            @php($np = 0)
                            @php($prev = '')
                            @foreach($payrolls as $p => $payroll)
                                <tr>
                                    @php($exp = explode(' - ', $payroll->period))
                                    <td>
                                        @php($next = $payroll->employee_id)
                                        @if($prev != $next)
                                            {{ $payroll->name }}
                                        @endif
                                        @php($prev = $payroll->employee_id)    
                                    </td>
                                    <td class="text-center">{{ $payroll->billing_number }}</td>
                                    <td class="text-center"><a href="/payroll/preview/{{ $payroll->payroll_number }}" target="_self">{{ $payroll->payroll_number }}</a></td> <!-- should be payroll_number -->
                                    <td class="text-center">{{ $exp[0] }}</td>
                                    <td class="text-center">{{ $exp[1] }}</td>
                                    <td class="text-right">
                                        @foreach($allowances as $key => $allowance)
                                            @if($key == $p)
                                                {{ number_format($allowance['food_allowance'], 2) }}
                                                @php($fa += $allowance['food_allowance'])
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-right">
                                        @foreach($allowances as $key => $allowance)
                                            @if($key == $p)
                                                {{ number_format($allowance['transportation_allowance'], 2) }}
                                                @php($ta += $allowance['transportation_allowance'])
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-right">
                                        @foreach($sss as $key => $s)
                                            @if($key == $p)
                                                {{ number_format($s['EE'], 2) }}
                                                @php($ss += $s['EE'])
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-right">
                                        @foreach($philhealth as $key => $phi)
                                            @if($key == $p)
                                                {{ number_format($phi['EE'], 2) }}
                                                @php($ph += $phi['EE'])
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-right">
                                        @foreach($pagibig as $key => $pgi)
                                            @if($key == $p)
                                                {{ number_format($pgi['EE'], 2) }}
                                                @php($pi += $pgi['EE'])
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-right">
                                        {{ number_format($payroll->netpay, 2) }}
                                        @php($np += $payroll->netpay)
                                    </td>
                                </tr>
                            @endforeach 
                        </tbody>
                        <tfoot>
                            <tr style="background:#CAE1FF">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><strong>{{ number_format($fa,2) }}</strong></td>
                                <td class="text-right"><strong>{{ number_format($ta,2) }}</strong></td>
                                <td class="text-right"><strong>{{ number_format($ss,2) }}</strong></td>
                                <td class="text-right"><strong>{{ number_format($ph,2) }}</strong></td>
                                <td class="text-right"><strong>{{ number_format($pi,2) }}</strong></td>
                                <td class="text-right"><strong>{{ number_format($np,2) }}</strong></td>
                            </tr>
                        </tfoot>
                    </table>        

                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            Showing {{ $payrolls->firstItem() }} to {{ $payrolls->lastItem() }} of {{ $payrolls->total() }} entries
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                {{ $payrolls->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection