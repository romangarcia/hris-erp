<div class="float-lg-right">
 <form action="?" method="get" class="form-inline justify-content-sm-center">
   <input type="hidden" value="{{$rows->currentPage()}}" name="page">
  <label>Show </label>
   <select class="form-control mx-2" name="perpage" onchange="this.form.submit()" style="width: auto;">
     <option value="10" {{app('request')->input('perpage') == 10 ? 'selected':''}}>10</option>
     <option value="25" {{app('request')->input('perpage') == 25 ? 'selected':''}}>25</option>
     <option value="50" {{app('request')->input('perpage') == 50 ? 'selected':''}}>50</option>
     <option value="100" {{app('request')->input('perpage') == 100 ? 'selected':''}}>100</option>
   </select>
   <label>entries</label>
 </form>
</div>