<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="{{@asset(get_favicon()) }}" /> 
  <title>@yield('title')</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="{{ asset('css/vendors/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/vendors/base/vendor.bundle.base.css') }}">
  <link rel="stylesheet" href="{{ asset('css/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
  <link rel="stylesheet" href="{{ asset('css/style.css') }}"> 
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}"> 

  <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}"> 
  <!-- <link rel="stylesheet" href="{{ asset('css/jquery.toast.min.css') }}">  -->
  @yield('customcss')

</head>
<body class="sidebar-fixed">  
  
  <div class="container-scroller">

    @if (Request::segment(1) != 'login' && Request::segment(1) != 'register' && Request::segment(1) != ''  && Request::segment(2) != 'reset' && Request::segment(1) != 'email')
      @include('layouts.topnav')
      <div class="container-fluid page-body-wrapper">
      @include('layouts.sidebar')
      <div class="main-panel">
        @yield('content')
        @include('layouts.footer')
      </div>
    @else
      <div class="container-fluid page-body-wrapper full-page-wrapper">
      @yield('content')
    @endif
        
    </div>
  </div>

  <script src="{{ asset('js/vendors/base/vendor.bundle.base.js') }}"></script>
  <!-- <script src="{{ asset('js/js/jquery.toast.min.js') }}"></script> -->
  <script src="{{ asset('js/vendors/chart.js/Chart.min.js') }}"></script>
  <script src="{{ asset('js/vendors/datatables.net/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('js/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
  <script src="{{ asset('js/js/off-canvas.js') }}"></script>
  <!--<script src="{{ asset('js/js/hoverable-collapse.js') }}"></script>-->
  <script src="{{ asset('js/js/template.js') }}"></script>
  <script src="{{ asset('js/js/dashboard.js') }}"></script>
  <script src="{{ asset('js/js/data-table.js') }}"></script>
  <script src="{{ asset('js/js/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('js/js/dataTables.bootstrap4.js') }}"></script>
  <script src="{{ asset('js/js/dropify.min.js') }}"></script>
  
  <script type="text/javascript">
    window.addEventListener("pageshow", function(event){
      if (event.originalEvent.persisted) {
        
            window.location.reload();
        }
    });
  </script>
  <!-- <script src="{{ asset('/js/js/toastDemo.js') }}"></script> -->
  <!-- <script src="{{ asset('/js//js/desktop-notification.js') }}"></script> -->

  @yield('customjs')
</body>
</html>