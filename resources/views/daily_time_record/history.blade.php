@extends('layouts.master')
@section('title', 'History')
@section('content')
<div class="content-wrapper">
    <div class="content">
        @include('includes.messages')
        <div class="card">
            <div class="card-header">
            Daily Time Record History: <b>{{ date("F Y") }}</b>
                <!-- <div class="row">
                    <div class="col-md-8" id="page-card_header">
                        Daily Time Record History: <b>{{ date("F Y") }}</b>
                    </div>
                    <div class="col-md-4">
                        <div class="dataTables_length float-right">
                            <label>
                                Show 
                                <select name="id-data_table_length" id="custom-data_table_length" aria-controls="id-data_table" class="form-control-sm">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select> 
                                entries
                            </label>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <select style="position: relative; z-index: 999;" class="form-control form-control-sm" name="employee_id" id="search_employee_id">
                                <option value="">Search Employee</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ ucwords($employee->first_name) }} {{ ucwords($employee->last_name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <!-- <input type="text" class="form-control form-control-sm" placeholder="Search Employee ID" name="employee_id" id="search_employee_id"> -->
                        <div class="form-group">
                            <input style="position: relative; z-index: 999;" id="date_range_picker" readonly type="text" placeholder="Choose Date Range" data-date-format="DD MMMM YYYY" class="form-control form-control-sm" name="date_range_picker">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <a style="position: relative; z-index: 999;" href="javascript:void(0);" class="btn btn-primary btn-icon-text btn-block btn-submit-search btn-sm">
                                <i class="mdi mdi-magnify"></i>
                                Search
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="form-group" id="4532">
                            <a style="position: relative; z-index: 999;" href="javascript:void(0);" class="btn btn-success btn-icon-text btn-block btn-sm btn-excel-download">
                                <i class="mdi mdi-arrow-down-bold-circle-outline"></i>
                                Export
                            </a>
                        </div>
                    </div>
                </div>
                <div class="" id="table_container">
                    <table class="table" id="id-data_table">
                        <thead>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Time-In</th>
                            <th>Time-Out</th>
                            <th>Total</th>
                            <th>Last Updated</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->

@endsection
@section('customjs')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">

  $(document).ready(function(){
      $('#date_range_picker[readonly]').css({'background-color':'#FFFFFF'});
      $('#date_range_picker').daterangepicker({
          showDropdowns: true,
          minYear: 1980,
          maxYear: parseInt(moment().format('YYYY')),
          locale: {
              cancelLabel: 'Clear'
          },
          "autoUpdateInput": false,
          "autoApply":false,
          maxDate:moment(),
      });
      $('#date_range_picker').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
      });
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    });


    $('#id-data_table').DataTable({
        searching: false,
        processing: true,
        serverSide: true,
        responsive: true,
        autoWidth : false,
        scrollX : true,
        language: {
            "paginate": {
              "previous": "<<",
              "next" : ">>"
            }
        },
        dom   : "<'row'<'col-sm-12 float-right margin-bottom20'B>><'row'<'col-sm-12 col-xs-12 col-md-6 exporttool'><'col-sm-12 col-xs-12 col-md-6 text-right mrt-sm-2'l>><'row'<'col-sm-12'tr>><'row'<'col-sm-5 col-xs-6'i><'col-sm-7 col-xs-6 justify-content-sm-center'p>>",
        ajax: {
            url : "{{ route('dtr.get_history') }}",
            type: 'GET',
            data: function (f) {
                f.employee_id = $('#search_employee_id').val();
                f.date = $('#date_range_picker').val();
            }
        },	  
        columns: [
            { data: 'date', sortable:true },
            { data: 'name', sortable:true },
            { data: 'time_in', sortable:true },
            { data: 'time_out', sortable:true },
            { data: 'total', sortable:true },
            { data: 'last_updated', sortable:true },
        ],
    });
    $('#id-data_table').DataTable().draw(true);
    $('.btn-submit-search').on('click',function(){
        $('#id-data_table').DataTable().draw(true);
    });
    {{--$("#4532").html('<a style="position: relative; z-index: 999;" href="{{ route('dtr.download_history_report') }}" class="btn btn-success btn-icon-text w-sm-100 btn-sm btn-excel-download">'+--}}
    {{--                        '<i class="mdi mdi-arrow-down-bold-circle-outline"></i>Export</a>');--}}

    $.fn.DataTable.ext.pager.numbers_length = 5;

      $('.btn-excel-download').on('click',function(){
          let query = {
              employee_id: $('#search_employee_id').val(),
              date: $('#date_range_picker').val(),
          };
          let uri = "{{URL::to('dtr/download_history_report')}}?" + $.param(query);

          window.location = uri;
      });
  });
</script>
@endsection