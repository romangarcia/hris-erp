@extends('layouts.master')
@section('title', 'Tardiness')
@section('content')
<div class="content-wrapper">
    <div class="content">
        @include('includes.messages')
        <div class="card">

            <div class="card-header">
				<div class="text-md-left float-md-left text-sm-left float-sm-left">Daily Time Record: <b>Tardiness</b> </div>
				
			</div>

            <div class="card-body">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-6">
					  <div class="form-group">
						<select style="position: relative; z-index: 999;" class="form-control form-control-sm" name="employee_id" id="search_employee_id">
							<option value="">Search Employee</option>
							@foreach($employees as $employee)
								<option value="{{ $employee->id }}">{{ ucwords($employee->first_name) }} {{ ucwords($employee->last_name) }}</option>
							@endforeach
						</select>
					  </div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-6">
					  <!-- <input type="text" class="form-control form-control-sm" placeholder="Search Employee ID" name="employee_id" id="search_employee_id"> -->
						<div class="form-group">	
					  		<input style="position: relative; z-index: 999;" id="date_range_picker" readonly type="text" placeholder="Choose Date Range" data-date-format="DD MMMM YYYY" class="form-control form-control-sm" name="date_range_picker"> 
						</div>	
					</div>
					<div class="col-md-2 col-sm-6 col-xs-12">
						<div class="form-group">	
						  <a style="position: relative; z-index: 999;" href="javascript:void(0);" class="btn btn-primary btn-icon-text btn-block btn-submit-search btn-sm">
							<i class="mdi mdi-magnify"></i>                                                    
							Search
						  </a>
						</div>	
					</div>
					 <div class="col-md-2 col-sm-6 col-xs-12">
						 <div class="form-group">	
						  <a style="position: relative; z-index: 999;" href="javascript:void(0);" class="btn btn-success btn-icon-text btn-block btn-sm btn-excel-download">
							<i class="mdi mdi-arrow-down-bold-circle-outline"></i>                                                    
							Export
						  </a>
						</div>	 
					</div>		
				  </div>	
				<div class=" table-responsive" id="tardiness_datatables_container">
                <table class="table" id="data_table_tardiness">
                    <thead>
                        <th>Date&nbsp;</th>
                        <th>Name&nbsp;</th>
                        <th>Time-In&nbsp;</th>
                        <th>Time-Out&nbsp;</th>
                        <th>Total&nbsp;</th>
                    </thead>
                   
                </table>
				</div>	
            </div>
        </div>
    </div>
</div>

<!-- Modal -->

@endsection

@section('javascript')

<!-- OPTIONAL SCRIPTS -->
<script src="/dist/plugins/chart.js/Chart.min.js"></script>
<script src="/dist/js/demo.js"></script>
<script src="/dist/js/pages/dashboard3.js"></script>


<!-- DataTables -->
<script src="/dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/dist/plugins/datatables/dataTables.bootstrap4.js"></script>
@endsection
@section('customjs')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript">
  var SITEURL = '{{URL::to('')}}';
  $('#date_range_picker[readonly]').css({'background-color':'#FFFFFF'});
  $('#date_range_picker').daterangepicker({
		showDropdowns: true,
		minYear: 1980,
		maxYear: parseInt(moment().format('YYYY')),
		locale: {
			  cancelLabel: 'Clear'
		},
		"autoUpdateInput": false,
		"autoApply":false,
		maxDate:moment(), 
  });

  $('#data_table_tardiness').DataTable({
    searching: false,
    processing: true,
    serverSide: true,
	responsive: true,
	autoWidth : false, 
    dom   : "<'row'<'col-sm-12 col-xs-12 text-right'l>><'row'<'col-sm-12 float-right margin-bottom20'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-5 col-xs-6'i><'col-sm-7 col-xs-6'p>>",
    ajax: {
        url : "{{ route('dtr.tardiness_list') }}",
        type: 'GET',
        data: function (f) {
          f.employee_id = $('#search_employee_id').val();
          f.date_range_picker = $('#date_range_picker').val();
        }
    },	  
    columns: [
        { data: 'date', sortable:false },
        { data: 'employee_name', sortable:true },
        { data: 'time_in', sortable:false },
        { data: 'time_out', sortable:false },
        { data: 'total', sortable:false }
    ]
	});

  /*if(window.matchMedia("(max-width: 992px)").matches){
      $("#tardiness_datatables_container").css("margin-top", "0px");
  }else{
      $("#tardiness_datatables_container").css("margin-top", "-75px");
  }*/

  $('#date_range_picker').on('apply.daterangepicker', function(ev, picker) {
		 $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
	});

  $(".btn-submit-search").on('click', function (){
    $('#data_table_tardiness').DataTable().draw(true);
  });
 $('.btn-excel-download').on('click',function(){
    var query = {
        search_employee_id: $('#search_employee_id').val(),
        date_range_picker: $('#date_range_picker').val(),
    }
    var url = "{{URL::to('dtr/excel_tardiness')}}?" + $.param(query)

   window.location = url;
});

</script>
@endsection