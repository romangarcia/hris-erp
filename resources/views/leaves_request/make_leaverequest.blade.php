<!-- create.blade.php -->

@extends('layouts.master')
@section('title', 'Request new leave')
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="content-wrapper">
  <div class="row">
    <div class="col grid-margin stretch-card">
      <div class="card">
        <div class="card-header">Request new leave</div>
        <div class="card-body">
          @if ($message = Session::get('success'))
          <div class="alert alert-success" role="alert">
            <i class="mdi mdi-alert-circle"></i>
            <strong>{{ $message }}</strong>
          </div>
          @endif

          @if ($errors->any())
          <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <i class="mdi mdi-alert-circle"></i>
            <strong>{{ $error }}</strong>
            @endforeach
          </div>
          @endif

 
          <form class="sample-form" method="POST" action="{{ route('leaves.leaverequest') }}" enctype="multipart/form-data">
            <div class="form-group">
              @csrf
              <div class="row">
                <div class="col-md-6 d-none">
                  <div class="form-group row">
                    <label class="col-sm-3">Date</label>
                    <div class="col-sm-9">
                      <input type="text" id="date_file" value="{{ $today }}" class="form-control" name="date_filed" readonly>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="date_start">Date Start</label>
                      <input type="date" id="date_start" data-date="" data-date-format="DD MMMM YYYY" class="form-control form-control-sm" name="date_start">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="date_end">Date End</label>
                      <input type="date" id="date_end" data-date="" data-date-format="DD MMMM YYYY" class="form-control form-control-sm" name="date_end">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <label for="name">Type of Leave</label>
                  <select class="form-control" name="type">
                    @foreach($leave_data as $row)
                    <option value="{{ $row->name }}">{{ $row->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <br />
              
              <label for="reason">Reasons</label>
              <textarea class="form-control" style="height: 150px;" aria-label="Reasons" name="reason"></textarea>
              
              <br />
              <div class="form-group">
                <div class="input-group">
                  <input type="file" class="form-control form-control-sm" placeholder="Name" name="select_file" autocomplete="off" style="text-transform: uppercase;">
                  <div class="input-group-append">
                  </div>
                </div>
              </div>
              <br />
              <button class="btn btn-success float-right w-sm-100" type="submit"><i class="mdi mdi-content-save"></i> SAVE</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
 
</div>



@endsection

