(function($) {
  'use strict';
  $(function() {
    var body = $('body');
    var contentWrapper = $('.content-wrapper');
    var scroller = $('.container-scroller');
    var footer = $('.footer');
    var sidebar = $('.sidebar');

    //Add active class to nav-link based on url dynamically
    //Active class can be hard coded directly in html file also as required

    function addActiveClass(element) {
      if (current === "") {
        //for root url
        if (element.attr('href').indexOf("index.html") !== -1) {
          element.parents('.nav-item').last().addClass('active');
          if (element.parents('.sub-menu').length) {
            element.closest('.collapse').addClass('show');
            element.addClass('active');
          }
        }
      } else {
        //for other url
        if (element.attr('href').indexOf(current) !== -1) {
          element.parents('.nav-item').last().addClass('active');
          if (element.parents('.sub-menu').length) {
            element.closest('.collapse').addClass('show');
            element.addClass('active');
          }
          if (element.parents('.submenu-item').length) {
            element.addClass('active');
          }
        }
      }
    }

    // var current = location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '');
    var current;
    var first_current_url = $(location).attr('pathname');
    first_current_url.indexOf(1);
    first_current_url.toLowerCase();
    current = first_current_url.split("/")[1];
    // alert(current);
    $('.nav li a', sidebar).each(function() {
      var $this = $(this);
      addActiveClass($this);
    })

    //Close other submenu in sidebar on opening any

    sidebar.on('show.bs.collapse', '.collapse', function() {
	  body.removeClass('sidebar-icon-only');
      sidebar.find('.collapse.show').collapse('hide');
    });


    //Change sidebar

    $('[data-toggle="minimize"]').on("click", function() {
      body.toggleClass('sidebar-icon-only');
    });

    //checkbox and radios
    $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');

    // Remove pro banner on close
    document.querySelector('#bannerClose').addEventListener('click',function() {
      document.querySelector('#proBanner').classList.add('d-none');
    });

  });

  //<- Start of RealTime Date/Clock->

    function real_time_topnav() {
        var months      = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        var days        = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
        var now         = new Date(); 
        var year        = now.getFullYear();
        var month       = months[now.getMonth()]; 
        var day         = now.getDate();
        var week_day    = days[now.getDay()-1];
        var hour        = now.getHours();
        var minute      = now.getMinutes();
        var second      = now.getSeconds();

        if(month.toString().length == 1) {
            month = '0'+month;
        }
        if(day.toString().length == 1) {
            day = '0'+day;
        }   
        if(hour.toString().length == 1) {
            hour = '0'+hour;
        }
        if(minute.toString().length == 1) {
            minute = '0'+minute;
        }
        if(second.toString().length == 1) {
            second = '0'+second;
        }   
        var dateTime = month+' '+day+', '+year+' '+week_day+' '+hour+':'+minute+':'+second+' PHT'; //year+'/'++'/'+day+' '+hour+':'+minute+':'+second;   
        return dateTime;
    }
    function load_realtime_topnav(){
      var currentTime = real_time_topnav();
      var time_container = $("#top_nav-current_realtime_date");

      if(window.matchMedia("(max-width: 992px)").matches){
        time_container.css('font-size', '11px');
        time_container.css('white-space', 'normal');
        time_container.css('display', 'block');
      }
      time_container.html(currentTime);
    }

    load_realtime_topnav();
    setInterval(function(){
      load_realtime_topnav();
    }, 1000);
  
  //<-/ End of RealTime Date/Clock->

})(jQuery);