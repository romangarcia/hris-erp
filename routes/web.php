<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return redirect('login');
});

Auth::routes();

Route::middleware(['auth', 'check_access'])->group(function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('departments', 'DepartmentController');
    Route::resource('leaves', 'LeaveController');
    Route::get('leaves_request/list', 'LeaveController@requestList')->name('leaves.leavelist');
    Route::get('leaves_request/layout', 'LeaveController@requestLeave')->name('leaves.leavepost');
    Route::get('leaves_request/layout/{leaf}/edit', 'LeaveController@requestEdit')->name('leaves.leaveedit');
    Route::post('leaves_request/leaverequest', 'LeaveController@requestPost')->name('leaves.leaverequest');
    Route::patch('leaves_request/layout/{leaf}', 'LeaveController@requestUpdate')->name('leaves.leaveupdate');
    Route::delete('leaves_request/{leaf}', 'LeaveController@requestDestroy')->name('leaves.leavedestroy');
    Route::get('leaves_request/list', 'LeaveController@requestList')->name('leaves.leavelist');
    Route::get('leaves_request/layout/{leaf}/edit', 'LeaveController@requestEdit')->name('leaves.leaveedit');
    Route::any('leaves_request/layout/create', 'LeaveController@create')->name('leaves.leavecreate');
    Route::get('leaves_request/filter_search', 'LeaveController@search')->name('leaves.leavesearch');
    Route::get('leaves_request/filter_search/list','LeaveController@resultlist');
    Route::get('leaves_request/search','LeaveController@search_filter')->name('leaves.leaves_search_filter');
    Route::any('leaves-list', 'LeaveController@leave_list')->name('leaves.leave_list');
    Route::get('leaves-list/edit/{id}', 'LeaveController@leave_edit')->name('leaves.leave_edit');

    Route::resource('sss', 'SocialSecurityController');
    Route::resource('tax', 'TaxController');
    Route::resource('philhealth', 'PhilhealthController');
    Route::resource('pagibig', 'PagibigController');

    Route::get('pagibigs/pagibig_list', 'PagibigController@pagibig_list');

    Route::get('employees/search', 'EmployeeController@employeeSearchView');
    Route::get('employees/searchEmp', 'EmployeeController@empSearch')->name('employee.empSearch');
    Route::resource('employees', 'EmployeeController');
    Route::any('generate_paystub/{emp_id}', 'EmployeeController@generatePDF');
    Route::any('generate_payroll/{emp_id}', 'EmployeeController@generatePDF1');


    Route::resource('attendance', 'AttendanceController');

    Route::get('roles/department/{id}',[
        'uses' => 'RoleController@edit_department',
        'as' => 'role.edit_department'
    ]);

    Route::get('roles/filter/department',[
        'uses' => 'RoleController@edit_department_filter',
        'as' => 'role.edit_department_filter'
    ]);


    Route::resource('roles', 'RoleController');
    Route::post('rolesenable', 'RoleController@enable')->name('role.enable');

    Route::resource('users', 'UserController');
    Route::post('user/isenable', 'UserController@isenable')->name('user.isenable');

    Route::resource('employment-status', 'EmploymentStatusController');
    // Route::post('employees/update', 'EmployeeController@update')->name('employees.update');
    // Route::get('employees/destroy/{id}', 'EmployeeController@destroy');
    Route::any('employees_list_ajax','EmployeeController@emp_list');

    Route::any('settings','UserController@settings');
    Route::any('user/change_password','UserController@change_password');
    Route::any('user/update_email','UserController@update_email');
    Route::any('user/update_username','UserController@update_username');
    Route::any('user/update_profile','UserController@update_profile');

    Route::get('dailytime',[
        'uses' => 'AttendanceController@dailyTime',
        'as' => 'dailytime'
    ]);

    Route::post('proccessDTR',[
        'uses' => 'AttendanceController@processDTR',
        'as' => 'proccessdtr'
    ]);

    Route::post('dtr/timein',[
        'uses' => 'AttendanceController@timeIn',
        'as' => 'proccessTimein'
    ]);

    Route::post('dtr/timeout',[
        'uses' => 'AttendanceController@timeOut',
        'as' => 'proccessTimeout'
    ]);

    Route::post('dtr/uploadcsv',[
        'uses' => 'AttendanceController@uploadCsv',
        'as' => 'uploadcsv'
    ]);

    Route::resource('payroll','PayrollController');
    Route::resource('holiday', 'HolidayController');

    Route::group(['prefix'=>'payroll'], function(){
        // Route::post('/generatess','PayrollController@generate')->name('payroll.generate');
        Route::get('/summary/{billing_number}/{employee_id}','PayrollController@summary')->name('payroll.summary');
        Route::get('/filter/search/query','PayrollController@search')->name('payroll.search');
        Route::any('/filter/search/input','PayrollController@search_filter')->name('payroll.search_filter');
        Route::any('/filter/generate_pdf/{payroll_id}','PayrollController@generate_pdf')->name('payroll.generate_pdf');
        Route::any('/filter/search_view_billing_number/{payroll_id}','PayrollController@search_view_billing_number')->name('payroll.search_view_billing_number');
        Route::get('/details/{payroll_id}','PayrollController@generate_pdf');
    });
    Route::any('bulk-payroll','PayrollController@index')->name('payroll.index');
    Route::any('bulk-payroll/generate', 'PayrollController@generate')->name('payroll.generate');

    Route::get('attendances/search','AttendanceController@searchIndex')->name('search.index');

    Route::resource('payrollledger', 'PayrollLedgerController');
    Route::post('/payrollledger/generate', 'PayrollLedgerController@generate');//->name('payrollledger.generate');
    Route::resource('overtime_request', 'OvertimeRequestController');
	Route::get('overtime_requests/filter_search','OvertimeRequestController@search')->name('overtime_request.search');
	Route::get('overtime_requests/search','OvertimeRequestController@search_filter')->name('overtime_requests.search_filter');

    Route::resource('company', 'CompanyController');
    Route::group(['prefix'=>'dtr'], function(){
        Route::resource('dtr', 'DailyTimeRecordController');
        Route::get('/history',
            [
                'uses'  => 'DailyTimeRecordController@history',
                'as'    => 'dtr.history'
            ]
        );
        Route::get('/get_history',
            [
                'uses'  => 'DailyTimeRecordController@get_history',
                'as'    => 'dtr.get_history'
            ]
        );
        Route::get('/search-filter',
            [
                'uses'  => 'DailyTimeRecordController@filter',
                'as'    => 'dtr.filter'
            ]
        );
        Route::any('/filter_list',
            [
                'uses'  => 'DailyTimeRecordController@filter_list',
                'as'    => 'dtr.filter_list'
            ]
        );

        Route::get('/tardiness',
            [
                'uses'  => 'DailyTimeRecordController@tardiness',
                'as'    => 'dtr.tardiness'
            ]
        );
        Route::get('/absences',
            [
                'uses'  => 'DailyTimeRecordController@absences',
                'as'    => 'dtr.absences'
            ]
        );
        Route::any('/absence_list',
            [
                'uses'  => 'DailyTimeRecordController@absence_list',
                'as'    => 'dtr.absence_list'
            ]
        );
        Route::get('/excel_tardiness',[
                'uses'  => 'DailyTimeRecordController@excel_tardiness',
                'as'    => 'dtr.excel_tardiness'
        ]);
        Route::any('/tardiness_list',
            [
                'uses'  => 'DailyTimeRecordController@tardiness_list',
                'as'    => 'dtr.tardiness_list'
            ]
        );
        Route::get('/download_filter_export',[
                'uses'  => 'DailyTimeRecordController@download_filter_export',
                'as'    => 'dtr.download_filter_export'
        ]);
        Route::get('/download_absence_excel',[
                'uses'  => 'DailyTimeRecordController@download_absence_excel',
                'as'    => 'dtr.download_absence_excel'
        ]);

        Route::get('/download_history_report',[
            'uses'  => 'DailyTimeRecordController@download_history_report',
            'as'    => 'dtr.download_history_report'
        ]);
    });

    Route::get('attendance/search/q','AttendanceController@searchIndex')->name('attendance.search');
    Route::any('role_list_ajax','RoleController@role_list');

    Route::get('/payrollledger/getDepartment/{id}','PayrollLedgerController@getDepartment');

    Route::get('user/search_filter','UserController@search_filter')->name('user.search_filter');
    Route::post('user/create_new_password','UserController@create_new_password')->name('user.create_new_password');
    Route::post('user/{id}/deactivate_user','UserController@deactivate_user')->name('user.deactivate_user');
    Route::post('user/{id}/activate_user','UserController@activate_user')->name('user.activate_user');

    Route::get('payrollledger/preview','PayrollLedgerController@generate')->name('payrollledger.preview');

    Route::get('holidays/search_filter','HolidayController@search_filter')->name('holiday.search_filter');

    Route::get('payroll/preview/{payroll_number}', 'PayrollLedgerController@previewUpdate');
    Route::get('payroll/print/{payroll_number}', 'PayrollController@search_view_billing_number');

    Route::resource('bank', 'BankController');
    Route::any('banks_list_ajax','BankController@bank_list');
    Route::get('bank/searchBank', 'BankController@bankSearch')->name('bank.bankSearch');


});

Route::middleware('auth')->group(function () {
    Route::get('employees/card/list',function(){
        return view('employees.list');
    })->name('emp.list');
    Route::get('attendance/card/list','AttendanceController@indexCard')->name('att.list');

});