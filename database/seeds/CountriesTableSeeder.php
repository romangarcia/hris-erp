<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Import the countries data
        $to_import = public_path('sql/countries.sql');
        DB::unprepared(file_get_contents($to_import));
    }
}
