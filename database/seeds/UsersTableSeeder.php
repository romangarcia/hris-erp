<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            ['name' => 'Admin',
            'created_at' => now(),
            'updated_at' => now(),
            ],
            ['name' => 'Human Resources',
            'created_at' => now(),
            'updated_at' => now(),
            ],
            ['name' => 'IT Department',
            'created_at' => now(),
            'updated_at' => now(),
            ]
        ]);

        DB::table('employment_statuses')->insert([
            'name' => 'REGULAR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        DB::table('leaves')->insert([
            'name' => 'VACATION LEAVE',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        DB::table('companies')->insert([
        [
            'company_name'      => 'Bentacos Information Technology Services',
            'email'             => 'business@bentacos.com',
            'address'           => 'Unit 2708 Tycoon Center Pearl Drive, Ortigas Center Pasig City 1605 Philippines',
            'created_at' => now(),
            'updated_at' => now(),
        ],
        ]);
        

        $roles = getRouteNames();
        for($i=1;$i<=3;$i++){
            foreach($roles as $key => $role){
                DB::table('roles')->insert([
                    // 'id' => 1,
                    'department_id' => $i,
                    'role' => 'full',
                    // 'role_name' => ($i==1) ? 'Administrator':'Human Resource',
                    'page' => $key,
                    'is_active' => '1',
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }
        }

        DB::table('users')->insert([
            'employee_id' => 1,
            'name' => 'Admin',
            'email' => 'test@test.com',
            'role' => '1',
            'password' =>  Hash::make('secret'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        DB::table('employees')->insert([
            'user_id' => 1,
            'employee_number' => 1,
            'last_name' => 'Admin',
            'first_name' => 'Bentacos',
            'email' => 'test@test.com',
            'department_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('users')->insert([
            'employee_id' => 2,
            'name' => 'Rey John',
            'email' => 'reyjhonbaquirin@yahoo.com',
            'password' =>  Hash::make('secret'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        DB::table('employees')->insert([
            'user_id'              => 2,
            'employee_number'      => '001906281',
            'first_name'           => 'Rey Jhon',
            'last_name'            => 'Baquirin',
            'middle_name'          => 'ABARRACOSO',
            'gender'               => 'M',
            'birthdate'            => '1996-02-08',
            'civil_status'         => 'Single',
            'address'              => 'Phase 6 Camarin Caloocan City',
            'email'                => 'reyjhonbaquirin@yahoo.com',
            'home_phone'           => '09193317525',
            'employment_status_id' => 1,
            'is_active'            => 0,
            'basic_salary'         => '15000.00',
            'tax_status'           => 'SME1',
            'department_id'        => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
        
        DB::table('attendances')->insert([
            ['date' => '2019-06-01',
                'employee_id' => '2',
                'name' => 'Rey Jhon A. Baquirin',
                'time_in' => '08:00 2019-06-01',
                'time_out' => '17:00 2019-06-01',
                'total' => 8.0,
                'created_at' => now(),
                'updated_at' => now()],
            ['date' => '2019-06-02',
                'employee_id' => '2',
                'name' => 'Rey Jhon A. Baquirin',
                'time_in' => '08:00 2019-06-02',
                'time_out' => '17:00 2019-06-02',
                'total' => 8.0,
                'created_at' => now(),
                'updated_at' => now()],
            ['date' => '2019-07-10',
                'employee_id' => '1',
                'name' => 'Admin',
                'time_in' => '08:00',
                'time_out' => '14:00',
                'total' => 6.0,
                'created_at' => now(),
                'updated_at' => now()],
            ['date' => '2019-07-9',
                'employee_id' => '1',
                'name' => 'Admin',
                'time_in' => '08:00',
                'time_out' => '16:00',
                'total' => 6.0,
                'created_at' => now(),
                'updated_at' => now()],
        ]);
        
        $limit = 30;
        $to_insert = '';
        
        for($x=1; $x <= $limit; $x++){
            if($x < 10){
                $x = '0'.$x;
            }
            DB::table('attendances')->insert([
                ['date' => '2019-07-'.$x,
                    'employee_id' => '2',
                    'name' => 'Rey Jhon Baquirin',
                    'time_in' => '08:00 2019-07-'.$x,
                    'time_out' => '17:00 2019-07-'.$x,
                    'total' => 8.0,
                    'created_at' => now(),
                    'updated_at' => now()],
            ]);
        }

        $min = 1750;
        $max = 0;
        $salary = 1500; //2000;
        $sss_er = 120; //160;
        $sss_ee = 60; //80;
        $sss_total = 240;
        $ec_er = 10;
        $er = 170;
        $ee = 80;
        $total = 36; //35;
        for($x=0; $x <= $total; $x++) {
            if($x==0){
                $min = -500;
            } elseif ($x==1) {
                $min = 1750;
                $max = 0;
            }
            $min += 500;
            $max = $x==0?2250:($min+499.99);
            $ec_er = ($sss_er + $sss_ee) >= 1740?30:10;
            
            DB::table('social_securities')->insert([
                [
                    'min' => $min,
                    'max' => $min >= 19750 ?999999:$max,
                    'salary' => $salary += 500,
                    'sss_er' => $sss_er += 40,
                    'sss_ee' => $sss_ee += 20,
                    'sss_total' => $sss_er + $sss_ee,
                    'sss_ec_er' => $ec_er,
                    'total_contribution_er' => $sss_er + $ec_er,
                    'total_contribution_ee' => $sss_ee,
                    'total_contribution_total' =>  $sss_er + $ec_er + $sss_ee,
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            ]);
        }

      DB::table('taxes')->insert([
          [
              'compensation_level'=>'0',
              'over' => '20833',
              'tax' => 0,
              'percentage' => 0,
              'created_at' => now(),
              'updated_at' => now()
          ],
          [
              'compensation_level'=>'20834',
              'over' => '33332',
              'tax' => 0,
              'percentage' => 20,
              'created_at' => now(),
              'updated_at' => now()
          ],
          [
              'compensation_level'=>'33333',
              'over' => '66666',
              'tax' => 2500,
              'percentage' => 25,
              'created_at' => now(),
              'updated_at' => now()
          ],
          [
              'compensation_level'=>'66667',
              'over' => '166666',
              'tax' => 10833.33,
              'percentage' => 30,
              'created_at' => now(),
              'updated_at' => now()
          ],
          [
              'compensation_level'=>'166667',
              'over' => '666666',
              'tax' => 40833.33,
              'percentage' => 32,
              'created_at' => now(),
              'updated_at' => now()
          ],
          [
              'compensation_level'=>'666667',
              'over' => '999999',
              'tax' => 200833.33,
              'percentage' => 35,
              'created_at' => now(),
              'updated_at' => now()
          ]
      ]);

        DB::table('philhealths')->insert([
           [
               'salary_bracket' => '0-10000',
               'salary_min' => '0.00',
               'salary_max' => '10000.00',
               'total_monthly_premium' => '275.00',
               'employee_share' => '137.50',
               'employer_share' => '137.50',
               'created_at' => now(),
               'updated_at' => now()
           ],
            [
                'salary_bracket' => '10000.1-40000',
                'salary_min' => '10000.1',
                'salary_max' => '40000.00',
                'total_monthly_premium' => '1100.00',
                'employee_share' => '550.00',
                'employer_share' => '550.50',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);

        DB::table('pagibigs')->insert([
            [
                'monthly_compensation' => '0-1500',
                'employee_share' => '1',
                'employer_share' => '2',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
            'monthly_compensation' => '1501-999999',
            'employee_share' => '2',
            'employer_share' => '2',
            'created_at' => now(),
            'updated_at' => now()
            ]
        ]);
    }
}
