<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Attendance extends Model
{
    /**
     * Return all generated employees from attendances
     * @return void
     */
    public function scopeEmployees($query, $date=array()) 
    {
        return $query->groupBy('employee_id')->whereBetween('date',$date)->get();
    }

    /**
     * Get the the total hours of an employee
     * @return void
     */
    public function scopeHours($query, $employee, $date=array())
    {
        return $query->select('employee_id','name', DB::raw('count(total) as total_days'))
                        ->where('employee_id',$employee)
                        ->whereBetween('date',$date)
                        ->first()->toArray();
    }

    // testing
}
