<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\Tax;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use DataTables;
use DateTime;
use Auth;
use Session;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee_id = Auth::user()->employee_id;
        $records = Attendance::paginate(10);
        $current_employee = Employee::findOrFail($employee_id);
        $employees = Employee::orderBy('first_name', 'ASC')->get();
        $filters = Session::get("attendfilters");
        $from_attandance_list = Session::get("from_attandance_list");

        return view('attendance.list',
            [
                'record'            =>  $records, 
                'employees'         =>  $employees,
                'current_employee'  =>  $current_employee,
                'filterdata'        =>  $filters,
                'from_attandance_list' => $from_attandance_list
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexCard(Request $request)
    {
        $perpage = $request->query('perpage',10);
       $rows = Attendance::where('date',date('Y-m-d'))->paginate(1);
        $entries = "<p>Showing ".$rows->firstItem()." to ".$rows->lastItem()." of ".$rows->total()." entries </p>";
        return view('attendance.attendancecard',
            [
                'rows' => $rows,
                'entries' => $entries,
                'total' => $rows->total()
            ]
        );
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $employees = Employee::orderBy('first_name', 'ASC')->get();
        return view('attendance.create', ['employees' => $employees]);
    }

    /**
     * handles the uploaded csv file
     */
    public function uploadCsv(Request $request)
    {
        $this->validate($request,[
            'csv'     => 'required'          
        ]);
        $file = $request->file('csv');
        if($file->getClientOriginalExtension()!= 'csv') {
          return redirect()->back()->with('error','Invalid File type!'); 
        }
        $data = $this->readCsv($file->getRealPath());
        $i=0;
        foreach($data as $record) {
            $date = date('Y-m-d', strtotime($record['date']));
            $timein = $record['for']=='timein'?date('H:i Y-m-d', strtotime($record['date'])):'';
            $c_timein = date('H:i',strtotime($timein));
            $night   = strtotime($c_timein) > strtotime(date('11:59')) ? true : false;
            if(Attendance::where(['date'=>$date,'employee_id'=>$record['id']])  
            ->first() === null && $record['for'] == 'timein') {
                DB::table('attendances')->insert([
                    'date' => $date,
                    'employee_id' => $record['id'],
                    'name' => $record['name'],
                    'time_in' => $timein,
                    'time_out' => '-',
                    'night_shift' => $night,
                    'total' => 0
                ]);
            }   
            if($record['for']=='timein' || $record['for']=='timeout') {
              $i++;
              if($i % 2 != 0){
                  $timein_ = date('H:i Y-m-d', strtotime($record['date']));
                  $con = $night;
              }
              if($i % 2 == 0) {
              $date = $con == true?date_format( DateTime::createFromFormat('Y-m-d', $date)->modify('-1 day'), 'Y-m-d'):$date;
                  $timeout = date('H:i Y-m-d', strtotime($record['date']));
                  DB::table('attendances')
                      ->where('employee_id', $record['id'])
                      ->where('date', $date)
                      ->update([
                          'time_out' => $timeout,
                          'total' => self::compute($timein_, $timeout, false)
                      ]);
              }
            } 
        }
        return redirect()->back()->with('success','Upload successfull!'); 
    }

    public function readCsv($file) 
    {
      date_default_timezone_set('Asia/Manila');
      $delimeter = $this->detectDelimiter($file);
			$fileread = fopen($file, "r");
			$datas = array();
			while (($getData = fgetcsv($fileread, 10000, $delimeter)) !== FALSE) {
				$data = array (
					
					'id' => $getData[0],
					'date' => $getData[1],
					'name' => $getData[4],
					'for' => $this->detectMethod($getData[5])
					
				);
				$datas[] = $data;
			}
      fclose($fileread);
      return $datas;
    }

    private function detectDelimiter($csvFile)
    {
      $delimiters = array(
          ';' => 0,
          ',' => 0,
          "\t" => 0,
          "|" => 0
      );
      $handle = fopen($csvFile,"r");
      $firstLine = fgets($handle);
      fclose($handle);
      foreach ($delimiters as $delimiter => &$count) {
          $count = count(str_getcsv($firstLine, $delimiter));
      }
      return array_search(max($delimiters), $delimiters);
    }

    private function detectMethod($method) 
    {
            if($method == 'I') {
                return 'timein';
            } else if($method == 'o') {
                return 'breakin';
            } else if($method == 'O') {
                return 'timeout';
            } else if($method == 'i') {
                return 'breakout';
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        date_default_timezone_set('Asia/Manila');
        $this->validate($request,[
            'date'     => 'required|date',
            'name'     => 'required', //'name'     => 'required|regex:/[A-Za-z\s\.\-]{5,50}$/', 
            'time_in'  => 'required'
        ]);

        $employee_id = $request->name;
        $get_employee = Employee::findOrFail($employee_id);
        $employee_name = $get_employee->first_name;
        $employee_name .= ' '. $get_employee->last_name;

        if($request->date == date('Y-m-d')){
            return redirect()->back()->with('error','This request is not allowed!'); 
        }
        
        $current_timeout = $request->time_out == null ?  date('H:i'): $request->time_out;
        
        $timein  = $request->time_in . " " . $request->date;
        $timeout = $current_timeout . " " . $request->date;
        $night   = $request->night_shift == 'on' ? true : false;
        
        $record           = new Attendance;
        $record->date     = $request->date;
        $record->employee_id = $employee_id; //Auth::user()->id;
        $record->name     = ucwords($employee_name);
        $record->time_in  = $request->time_in ?? "-";
        $record->time_out = $request->time_out ?? "-";
        $record->total = $request->time_out == null ? 0:self::compute($timein, $timeout, $night);
        $record->night_shift = $night;
        $record->save(); 

        return redirect('attendance')->with('success','Record added successfully!'); 
    }

    public function timeIn(Request $request)
    {
        if(Attendance::where(['date'=>date('Y-m-d'),'employee_id'=>Auth::user()->id])  
        ->first() != null) {
            return redirect()->back()->with('error','Your time stamp is already active.'); 
        }
        date_default_timezone_set('Asia/Manila');
        $night   = strtotime(date('H:i')) > strtotime(date('11:59')) ? true : false;
        $timein_real = date('H:i Y-m-d');
        $record = new Attendance;
        $record->date = date('Y-m-d');
        $record->employee_id = Auth::user()->employee_id;
        $record->name     = ucwords(Auth::user()->name);
        $record->time_in  = $timein_real;
        $record->time_out = '-';
        $record->total = 0.0;
        $record->night_shift = $night;
        $record->save();
        Session::put('attendance_id',$record->id);

        Session::flash("flash_attendance", array('employee_id' => $record->employee_id, 'date' => $record->date));
        
        return redirect(route('attendance.index'))->with('success','Time-in successfully! at '.$timein_real);
    }

    public function timeOut(Request $request)
    {
      date_default_timezone_set('Asia/Manila');
      
      $timeout = date('H:i Y-m-d');
      $record = Attendance::find($request->att_id);
        if($record==null || $record->time_out != '-'){
            return redirect()->back()->with('error','This request is not allowed. You may not have been time-in for this day or you are trying multiple request.');
        }
      $timeou_real = date('H:i Y-m-d');
      $record->time_out = $timeou_real;
      $current_timein = $record->time_in;
      $record->total = self::compute($current_timein, $timeout, false);
      $record->save();

      Session::flash("flash_attendance", array('employee_id' => $record->employee_id, 'date' => $record->date));
      
      return redirect(route('attendance.index'))->with('success','Time-out successfully! at '.$timeou_real);
    }

    /**
     * Compute the interval hours between two input times
     * @param datetime
     * @return double
     */
    private static function compute($timein, $timeout, $night)
    {
        $a = DateTime::createFromFormat('H:i Y-m-d', $timein);
        if($night) {
            $b = DateTime::createFromFormat('H:i Y-m-d', $timeout)->modify('+1 day');
        } else {
            $b = DateTime::createFromFormat('H:i Y-m-d', $timeout);
        }
        $interval = $a->diff($b);
        $hours    = ($interval->days * 24) + $interval->h
                    + ($interval->i / 60) + ($interval->s / 3600);
            if($hours >= 8) {
                $total = $hours - 1;
            } else {
                $total = $hours;
            }
        return $total;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        $employees = Employee::orderBy('first_name', 'ASC')->get();
        $records = Attendance::findOrFail($attendance->id);

        $filters = Session::get("attendfilters");

        if(is_array($filters)){
          Session::flash("attendfilters", $filters);
        }
        Session::flash("from_attandance_list", 1);

        return view('attendance.edit', ['record' => $records, 'employees' => $employees]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        $this->validate($request,[
            'date'     => 'required|date',
            'name'     => 'required', //'required|regex:/[A-Za-z\s\.\-]{5,50}$/',
            'time_in'  => 'required',
        ]);

        $current_timeout = $request->time_out == null ?  date('H:i'): $request->time_out;

        $timein  = $request->time_in . " " . $request->date;
        $timeout = $current_timeout . " " . $request->date;
        $night   = $request->night_shift == 'on' ? true : false;
        
        $total = $request->time_out == null ? 0 : self::compute($timein, $timeout, $night);

        $employee_id = $request->name;
        $get_employee = Employee::findOrFail($employee_id);

        $record = Attendance::find($attendance->id);
        $record->date     = $request->date;
        $record->employee_id = $employee_id; //Auth::user()->id;
        $record->name     = $get_employee->first_name.' '.$get_employee->last_name;
        $record->time_in  = $request->time_in ?? "-";
        $record->time_out = $request->time_out ?? "-";
        $record->total = $total;
        $record->night_shift = $night;
        if($record->save()){
            Session::flash('attendance_updated', $attendance->id); 
            return redirect(route('attendance.index'))->with('success','Record updated successfully!'); 
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        if(Attendance::destroy($attendance->id)){
            return redirect()->back()->with('success','Record deleted successfully!');
        } else {
            return redirect()->back()->with('error','Request Failed!');
        }
        
    }

    public function dailyTime()
    {
        return view('attendance.dailytime');
    }
    
    public function processDTR(Request $request)
    {
        date_default_timezone_set('Asia/Manila');
        
        $method = $request->method;
        if($method == 'timein') {
            $timein = date('H:i');

            $record = new Attendance;
            $record->date = date('Y-m-d');
            $record->employee_id = $request->employeeID;
            $record->name = $request->employeeID;
            $record->time_in = $timein;
            $record->time_out = "-";
            $record->total = "0";
            $record->night_shift = false;
            if($record->save()){
                $data = array (
                'error'=>'',
                'message'=> 'Hello '. $request->employeeID . ' You have successfully ' . $request->method,
                'employeeID' => $request->employeeID,
                'time'=>$timein
                );
            }
        }
        return json_encode($data);
    
    }

    public function searchIndex(Request $request)
    {
        // $employee_id = Auth::user()->employee_id;

        $employee_id = $request->employee_id;
        $date_range = $request->date_range_pick;

        $postbtn = $request->postbtn;
        $from_attandance_list = $request->from_attandance_list;
        
        $select_qry = array(
            'employees.first_name',
            'employees.last_name',
            'attendances.id as r_id',
            'attendances.time_in',
            'attendances.time_out',
            'attendances.total',
            'attendances.night_shift',
            'attendances.updated_at',
            'attendances.date',
        );

        $filters = [];

        if($employee_id != ''){
            $filters['employee_id'] = $employee_id;
        }
        if($date_range != ''){
            $filters['date_range'] = $date_range;
        }

        if($postbtn)
        {
            Session::put("attendfilters", $filters);
        }elseif($from_attandance_list){
          Session::flash("attendfilters", $filters);
        }else
        {
            Session::forget("attendfilters");
        }

        if($employee_id != '' && $date_range != ''){

              $date_array  = explode(" - ",$date_range);
              $from = date('Y-m-d',strtotime($date_array[0]));
              $to = date('Y-m-d',strtotime($date_array[1]));

            $data = Attendance::whereBetween('attendances.date',[$from,$to])
            ->where('employee_id', $employee_id)
            ->join('employees', 'employees.id',  '=', 'attendances.employee_id')
            ->orderBy('attendances.date', 'desc')
            ->get($select_qry);
        }
        else if($employee_id != ''){

            $data = Attendance::where('employee_id', $employee_id)
            ->join('employees', 'employees.id',  '=', 'attendances.employee_id')
            ->orderBy('attendances.date', 'desc')
            ->get($select_qry);

        }else if($date_range != ''){

            $date_array  = explode(" - ",$date_range);
            $from = date('Y-m-d',strtotime($date_array[0]));
            $to = date('Y-m-d',strtotime($date_array[1]));

            $data = Attendance::whereBetween('attendances.date',[$from,$to])
            ->join('employees', 'employees.id',  '=', 'attendances.employee_id')
            ->orderBy('attendances.date', 'desc')
            ->get($select_qry);
        }else{
            $data = Attendance::join('employees', 'employees.id',  '=', 'attendances.employee_id')
            ->orderBy('attendances.date', 'desc')
            ->get($select_qry);
        }

        

        $data_tables = Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('id', function($row){
                
                $url_edit = route('attendance.edit', [$row->r_id]);
                $response = '<a href="'.$url_edit.'">'.$row->r_id.'</a>';
                return $response;
            })
            ->addIndexColumn()
            ->addColumn('date', function($row){

                $response = $row->date;
                if($row->night_shift == 1){
                    $response .= '<small><span class="badge badge-dark">night</span></small';
                }

                return $response;
            })
            ->addIndexColumn()
            ->addColumn('name', function($row){
                return ucwords($row->first_name).' '.ucwords($row->last_name);
            })
			->addIndexColumn()
            ->addColumn('time_in', function($row){
                return date('H:i', strtotime($row->time_in));
            })
			->addIndexColumn()
            ->addColumn('time_out', function($row){
                return date('H:i', strtotime($row->time_out));
            })
            ->addIndexColumn()
            ->addColumn('total', function($row){
                $response = '00:00:00';

                if($row->time_out != '-'){
                    $total_time = 60 * $row->total;
                    $response = gmdate("H:i:s", $total_time);
                }

                return $response;
            })
			->addIndexColumn()
            ->addColumn('last_updated', function($row){
                $response = '';
                if($row->updated_at != ''){
                    $response = date("Y-m-d", strtotime($row->updated_at));
                }
                return $response;
            })
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $url_edit = route('attendance.edit', [$row->r_id]);
                $action = '<td><button type="button" onclick="window.location.href=\''.$url_edit.'\'" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm"><i style="margin-left: -6px;" class="mdi mdi-lead-pencil"></i></button> </td>';
                $action .= '<td><button type="button" data-toggle="modal" data-id="'.$row->r_id.'" data-target="#DeleteModal" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm btn-delete_row ml-4"><i style="margin-left: -7px;" class="mdi mdi-delete"></i></button></td>';
                
                return $action;
            })
            ->rawColumns(['id', 'date','name','time_in','time_out','total', 'last_updated', 'action'])
            ->make(true);
        
        return $data_tables;
    }

}
