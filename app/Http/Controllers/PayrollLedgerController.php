<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\Department;
use App\Payroll;
Use App\Attendance;
use App\Holiday;
Use App\OvertimeRequests as ot;
use DB;
use Session;
use Illuminate\Support\Facades\Input;

class PayrollLedgerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        $departments = Department::all();

        //return payrolls based on payroll_type = 1
        $payrolls = Payroll::where('payroll_type', 1)
                        ->orderBy('id', 'desc')
                        ->paginate(10);
                        
        return view('payroll_ledger.index', compact('employees', 'departments', 'payrolls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //dump(session()->all());
        //dd($request);
        // query first employees table to verify employee and department
        $employee = Employee::where('user_id', $request->employee_id);
                        if($request->has('department_id') && $request->department_id != ''){
                            $employee = $employee->where('department_id', $request->department_id);
                        }
                            $employee = $employee->first();
        //return list of all employees for dropdown only
        $employees = Employee::orderBy('last_name')
                        ->select('id', 'employee_number', 'first_name', 'middle_name', 'last_name', 'address')
                        ->get();
        // if true, query the attendance table
        if($employee) {
            $days        = Attendance::Hours($request->employee_id, [$request->from,$request->to]);
            $total_hours = $days['total_days'] * 8;
            $overtime = ot::Overtime($request->employee_id, [$request->from,$request->to]);
            $attendances = Attendance::where('employee_id',$request->employee_id)
                            ->whereBetween('date',[$request->from,$request->to])->get();
            $payroll = computePayroll($request->employee_id, $total_hours, $overtime, $attendances);
            //additional
            $payroll['total_hours'] = $total_hours;
            $payroll['payroll_date'] = $request->payroll_date;
            $payroll['period_from'] = $request->from;
            $payroll['period_to'] = $request->to;
            $payroll['exists'] = 0;
            $payroll['payroll_number'] = $this->generateBillAndPayrollNumber()->payroll_number;
            if ($request->ajax()) {
                return response()->json(array($payroll, $employee, $employees));
            } else {
                return view('payroll_ledger.preview', compact('employee', 'payroll', 'employees'));
            }
        } 
        else {
            
            return redirect()->back()->with('success','Payroll ' . Session('payroll_number') . ' successfully saved.')->withInput(Input::except('payroll_number')); 
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $employee_id = $request->employee_id;
        $department_id = $request->department_id;
        $payroll_date = $request->updated_payroll_date ?? $request->payroll_date;
        $period_from = $request->updated_period_from ?? $request->period_from;
        $period_to = $request->updated_period_to ?? $request->period_to;

        $period = ($request->updated_period_from != '' && $request->updated_period_to != '') ? $request->updated_period_from . ' - ' . $request->updated_period_to : $request->period;
        
        $perpage = $request->query('perpage',10);

        // dump($payroll_date);
        // dump($period_from);
        // dd($period_to);

        $data['employee_id'] = $request->employee_id;
        $data['name'] = $request->employee_name;
        $data['total_hours'] = $request->total_hours;
        $data['period'] = $request->period;
        $data['gross'] = $request->ee_gross;

        $data['days'] = array(
            'regular_holidays' => $request->regular_holidays,
            'regular_holiday_pay' => number_format($request->ee_regular_holiday_pay,2,'.',''),
            'special_holidays' => $request->special_holidays,
            'special_holiday_pay' => number_format($request->ee_special_holiday_pay,2,'.',''),
            'restday_days' => $request->restday_days,
            'restday_days_pay' => number_format($request->ee_restday_days_pay,2,'.',''),
            'rest_and_holiday_hours' => $request->rest_and_holiday_hours
        );
        $data['allowances'] = array(
            'food_allowance' => $fa = number_format($request->ee_food_allowance,2,'.',''),
            'transportation_allowance' => $ta = number_format($request->ee_transportation_allowance,2,'.',''),
            'personal_allowance' => $pa = number_format($request->ee_personal_allowance,2,'.',''),
            'total_allowances' => number_format($fa + $ta + $pa,2,'.',''),
        );
        $data['overtimes'] = array(
            'total_hours' => $request->ot_total_hours,
            'special_ot_pay' => number_format($request->ee_special_ot_pay,2,'.',''),
            'regular_ot_pay' => number_format($request->ee_regular_ot_pay,2,'.',''),
            'restday_ot_pay' => number_format($request->ee_restday_ot_pay,2,'.',''),
            'total_overtime_pay' => number_format($request->total_overtime_pay,2,'.',''),
            'special_holiday_hours' => $request->special_holiday_hours,
            'regular_holiday_hours' => $request->regular_holiday_hours,
            'rest_day_hours' => $request->rest_day_hours
        );
        $data['sss'] = array(
            'EE' => $ee = number_format($request->ee_sss * -1,2,'.',''), // -1 is representation only on preview
            'ER' => $er = number_format($request->er_sss,2,'.',''),
            'total_contribution' => number_format($ee + $er,2,'.','')
        );
        $data['philhealth'] = array(
            'EE' => $ee = number_format($request->ee_philhealth * -1,2,'.',''), // -1 is representation only on preview
            'ER' => $er = number_format($request->er_philhealth,2,'.',''),
            'total_contribution' => number_format($ee + $er,2,'.','')
        );
        $data['pagibig'] = array(
            'EE' => $ee = number_format($request->ee_pagibig * -1,2,'.',''), // -1 is representation only on preview
            'ER' => $er = number_format($request->er_pagibig,2,'.',''),
            'total_contribution' => number_format($ee + $er,2,'.','')
        );
        $data['total_deduction'] = number_format($request->ee_deductions,2,'.','');
        $data['netpay'] = number_format($request->ee_net,2,'.','');
        $data['tax'] = array(
            'non_taxable_income' => number_format($request->ee_non_taxable_income,2,'.',''),
            'taxable_income' => number_format($request->ee_taxable_income,2,'.',''),
            'witholding_tax' => number_format($request->ee_witholding_tax,2,'.','')
        );
        $data['payroll_type'] = 1;
        $data['payroll_date'] = $request->payroll_date;
        $data['is_paid'] = $request->is_paid == NULL ? 0 : 1;
        $data['description'] = $request->description;
        $data['notes'] = $request->notes;

        // before insert, lets verify if same record exists
        // PARAMS: employee_id, period
//        $verify_record = Payroll::where('employee_id', $data['employee_id'])
//                    ->where('period', $data['period'])
//                    ->first();
//
//        if(isset($verify_record) && $verify_record->id != '') {
//            //update
//            $update = array(
//                'period' => $data['period'],
//                'total_hours' => $data['total_hours'],
//                'days' => json_encode($data['days']),
//                'overtimes' => json_encode($data['overtimes']),
//                'allowances' => json_encode($data['allowances']),
//                'gross' => $data['gross'],
//                'sss' => json_encode($data['sss']),
//                'philhealth' => json_encode($data['philhealth']),
//                'pagibig' => json_encode($data['pagibig']),
//                'total_deduction' => $data['total_deduction'],
//                'basic_pay' => $data['gross'],
//                'tax' => json_encode($data['tax']),
//                'netpay' => $data['netpay'],
//                // 'payroll_type' => $data['payroll_type'], // (???)
//                'payroll_date' => $data['payroll_date'],
//                'description' => $data['description'],
//                'notes' => json_encode($data['notes']),
//                'is_paid' => $data['is_paid'],
//                'revision' => $verify_record->revision + 1
//            );
//            Payroll::where('id', $verify_record->id)
//                    ->update($update);
//
//            return redirect('/payrollledger')->with('success','Payroll for ' . $verify_record->name . ' for the period of ' . $data['period'] . ' was updated successfully!');
//
//        } else {
            //insert || save
            $nr = new Payroll;
            $nr->billing_number = $this->generateBillAndPayrollNumber()->billing_number;
            $nr->payroll_number = $this->generateBillAndPayrollNumber()->payroll_number;
            $nr->employee_id = $data['employee_id'];
            $nr->name = $data['name'];
            $nr->period = $data['period'];
            $nr->total_hours = $data['total_hours'];
            $nr->days = json_encode($data['days']);
            $nr->overtimes = json_encode($data['overtimes']);
            $nr->allowances = json_encode($data['allowances']);
            $nr->gross = $data['gross'];
            $nr->sss = json_encode($data['sss']);
            $nr->philhealth = json_encode($data['philhealth']);
            $nr->pagibig = json_encode($data['pagibig']);
            $nr->total_deduction = $data['total_deduction'];
            $nr->basic_pay = $data['gross'];
            $nr->tax = json_encode($data['tax']);
            $nr->netpay = $data['netpay'];
            $nr->payroll_type = $data['payroll_type'];
            $nr->payroll_date = $data['payroll_date'];
            $nr->description = $data['description'];
            $nr->notes = $data['notes'];
            $nr->is_paid = $data['is_paid'];
            if($nr->save()){
                $perpage = $request->perpage ?? 10;
                $payrolls = Payroll::where('payroll_number', $nr->payroll_number)->where('billing_number', $nr->billing_number)->paginate($perpage);
                $allowances=[];   
                $overtime=[];     
                $sss=[];          
                $philhealth=[];   
                $pagibig=[];      
                $tax=[];          
                $wdays=[];   
                foreach($payrolls as $payroll){
                    $allowances[]   = json_decode($payroll->allowances,true);
                    $overtime[]     = json_decode($payroll->overtimes,true);
                    $sss[]          = json_decode($payroll->sss,true);
                    $philhealth[]   = json_decode($payroll->philhealth,true);
                    $pagibig[]      = json_decode($payroll->pagibig,true);
                    $tax[]          = json_decode($payroll->tax,true);
                    $wdays[]        = json_decode($payroll->days,true);
                }
            // return view('payroll.list', compact('payrolls', 'allowances', 'overtime', 'sss', 'philhealth', 'pagibig', 'tax', 'wdays', 'employee_id', 'department_id', 'payroll_date', 'period_from', 'period_to'));

            // $current_request = array(
            //     'currentEmployeeId' => $employee_id,
            //     'currentDepartmentId' => $department_id,
            //     'currentPayrollDate' => $payroll_date,
            //     'currentFrom' => $period_from,
            //     'currentTo' => $period_to
            // );

            // dd($nr->payroll_number);
            //return redirect()->route('payrollledger.create')->withInput(Input::all());
            Session(['payroll_number' => $nr->payroll_number]);
            return redirect()->route('payrollledger.create')->withInput(Input::except('payroll_number'))->with('success', 'Payroll successfully saved.');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payroll = Payroll::join('employees', 'employees.id','=','payrolls.employee_id')
                    ->where('payrolls.id', $id)
                    ->where('payrolls.payroll_type', 1)
                    ->select('payrolls.*', 'employees.employee_number', 'employees.basic_salary', 'employees.address')
                    ->firstOrFail();

        $allowances   = json_decode($payroll->allowances,true);
        $overtime     = json_decode($payroll->overtimes,true);
        $sss          = json_decode($payroll->sss,true);
        $philhealth   = json_decode($payroll->philhealth,true);
        $pagibig      = json_decode($payroll->pagibig,true);
        $tax          = json_decode($payroll->tax,true);
        $wdays        = json_decode($payroll->days,true);

        //dd($payroll);

        return view('payroll_ledger.show', compact('payroll','allowances','overtime','sss','philhealth','pagibig','tax','wdays'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // dd($request);
        $data['employee_id'] = $request->employee_id;
        $data['name'] = $request->employee_name;
        $data['total_hours'] = $request->total_hours;
        $data['period'] = ($request->updated_period_from != '' && $request->updated_period_to != '') ? $request->updated_period_from . ' - ' . $request->updated_period_to : $request->period;
        $data['gross'] = $request->ee_gross;

        $data['days'] = array(
            'regular_holidays' => $request->regular_holidays,
            'regular_holiday_pay' => number_format($request->ee_regular_holiday_pay,2,'.',''),
            'special_holidays' => $request->special_holidays,
            'special_holiday_pay' => number_format($request->ee_special_holiday_pay,2,'.',''),
            'restday_days' => $request->restday_days,
            'restday_days_pay' => number_format($request->ee_restday_days_pay,2,'.',''),
            'rest_and_holiday_hours' => $request->rest_and_holiday_hours
        );
        $data['allowances'] = array(
            'food_allowance' => $fa = number_format($request->ee_food_allowance,2,'.',''),
            'transportation_allowance' => $ta = number_format($request->ee_transportation_allowance,2,'.',''),
            'personal_allowance' => $pa = number_format($request->ee_personal_allowance,2,'.',''),
            'total_allowances' => number_format($fa + $ta + $pa,2,'.',''),
        );
        $data['overtimes'] = array(
            'total_hours' => $request->ot_total_hours,
            'special_ot_pay' => number_format($request->ee_special_ot_pay,2,'.',''),
            'regular_ot_pay' => number_format($request->ee_regular_ot_pay,2,'.',''),
            'restday_ot_pay' => number_format($request->ee_restday_ot_pay,2,'.',''),
            'total_overtime_pay' => number_format($request->total_overtime_pay,2,'.',''),
            'special_holiday_hours' => $request->special_holiday_hours,
            'regular_holiday_hours' => $request->regular_holiday_hours,
            'rest_day_hours' => $request->rest_day_hours
        );
        $data['sss'] = array(
            'EE' => $ee = number_format($request->ee_sss * -1,2,'.',''), // -1 is representation only on preview
            'ER' => $er = number_format($request->er_sss,2,'.',''),
            'total_contribution' => number_format($ee + $er,2,'.','')
        );
        $data['philhealth'] = array(
            'EE' => $ee = number_format($request->ee_philhealth * -1,2,'.',''), // -1 is representation only on preview
            'ER' => $er = number_format($request->er_philhealth,2,'.',''),
            'total_contribution' => number_format($ee + $er,2,'.','')
        );
        $data['pagibig'] = array(
            'EE' => $ee = number_format($request->ee_pagibig * -1,2,'.',''), // -1 is representation only on preview
            'ER' => $er = number_format($request->er_pagibig,2,'.',''),
            'total_contribution' => number_format($ee + $er,2,'.','')
        );
        $data['total_deduction'] = number_format($request->ee_deductions,2,'.','');
        $data['netpay'] = number_format($request->ee_net,2,'.','');
        $data['tax'] = array(
            'non_taxable_income' => number_format($request->ee_non_taxable_income,2,'.',''),
            'taxable_income' => number_format($request->ee_taxable_income,2,'.',''),
            'witholding_tax' => number_format($request->ee_witholding_tax,2,'.','')
        );
        $data['payroll_type'] = 1;
        $data['payroll_date'] = $request->updated_payroll_date != '' ? $request->updated_payroll_date : $request->payroll_date;
        $data['is_paid'] = $request->is_paid == NULL ? 0 : 1;
        $data['description'] = $request->description;
        $data['notes'] = $request->notes;

        $nr = Payroll::findOrFail($id);
        $nr->period = $data['period'];
        $nr->payroll_date = $data['payroll_date'];
        $nr->days = json_encode($data['days']);
        $nr->overtimes = json_encode($data['overtimes']);
        $nr->allowances = json_encode($data['allowances']);
        $nr->gross = $data['gross'];
        $nr->sss = json_encode($data['sss']);
        $nr->philhealth = json_encode($data['philhealth']);
        $nr->pagibig = json_encode($data['pagibig']);
        $nr->total_deduction = $data['total_deduction'];
        $nr->basic_pay = $data['gross'];
        $nr->tax = json_encode($data['tax']);
        $nr->netpay = $data['netpay'];
        $nr->description = $data['description'];
        $nr->notes = $data['notes'];
        $nr->is_paid = $data['is_paid'];
        $nr->revision = $nr->revision + 1;
        $nr->save();

        return redirect()->back()->with('success','Record updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Generate Payroll Data for all valid employees
     * @param $request - Date range to be generated
     * @return void
     */
    public function generate(Request $request)
    {
        // query first employees table to verify employee and department
        $employee = Employee::where('user_id', $request->employee_id);
                        if($request->has('department_id') && $request->department_id != ''){
                            $employee = $employee->where('department_id', $request->department_id);
                        }
                            $employee = $employee->first();
        //return list of all employees for dropdown only
        $employees = Employee::orderBy('last_name')
                        ->select('id', 'employee_number', 'first_name', 'middle_name', 'last_name', 'address')
                        ->get();
        // if true, query the attendance table
        if($employee) {
            $days        = Attendance::Hours($request->employee_id, [$request->from,$request->to]);
            $total_hours = $days['total_days'] * 8;
            $overtime = ot::Overtime($request->employee_id, [$request->from,$request->to]);
            $attendances = Attendance::where('employee_id',$request->employee_id)
                            ->whereBetween('date',[$request->from,$request->to])->get();
            $payroll = computePayroll($request->employee_id, $total_hours, $overtime, $attendances);
            //additional
            $payroll['total_hours'] = $total_hours;
            $payroll['payroll_date'] = $request->payroll_date;
            $payroll['period_from'] = $request->from;
            $payroll['period_to'] = $request->to;
            $payroll['exists'] = 0;
            $payroll['payroll_number'] = $this->generateBillAndPayrollNumber()->payroll_number;
            if ($request->ajax()) {
                return response()->json(array($payroll, $employee, $employees));
            } else {
                return view('payroll_ledger.preview', compact('employee', 'payroll', 'employees'));
            }
        } else {
            return redirect()->back()->with('error','Employee record not found'); 
        }
    }


    /**
     * Generate Bill number
     * @return string $bill_number
     */
    private function generateBillNumber($new=null)
    {
        $bill             = Payroll::orderby('id', 'desc')->first();
        $bill_number_init = $bill != null ? explode('-',$bill->billing_number) : array(0=>'BITS',1=>0);
        $bill_number_new  = $new == null ? $bill_number_init[1] + 1 : $bill_number_init[1];
        $bill_number      = "$bill_number_init[0]-$bill_number_new";
        return $bill_number;
    }


    public function getDepartment($id)
    {
        $departments = Department::findOrFail($id);
        $departments->employee_id = $id;
        return json_encode($departments);
    }

    private function generateBillAndPayrollNumber()
    {
        $bp = Payroll::orderBy('id', 'desc')->first();
        //dd($bp);
        $prefix = 'BITS';
        $middle = date('ym');        
        
        // PAYROLL NUMBER
        if($bp == NULL || $bp->payroll_number == NULL){
            $bp = new Payroll;
            $bp->payroll_number = $prefix . '-' . $middle . '-0001';
        } else { 
            do {
                $bp = Payroll::orderBy('id', 'desc')->first();
                $exploaded = explode('-', $bp->payroll_number);
                $last_digit = $exploaded[2] + 1;
                if($exploaded[2] == 9999){ 
                    $new_last_number = '0001';
                } else {
                    $new_last_number = str_pad($last_digit, 4, '0', STR_PAD_LEFT);            
                }
                $bp->payroll_number = $prefix . '-' . $middle . '-' . $new_last_number;
                $chk = Payroll::where('payroll_number', $bp->payroll_number)->first();
            } 
            while($bp->payroll_number == $chk);
        }
        
        // BILLING NUMBER
        if($bp->billing_number != null) {
            $exploaded = explode('-', $bp->billing_number);
            $bp->billing_number = $prefix . '-' . ($exploaded[1] + 1);
        } else {
            $bp->billing_number = $prefix . '-' . 1;
        }
        return $bp;
    }

    public function previewUpdate($payroll_number)
    {
        // dd($payroll_number);
        if($payroll_number != NULL){
            $payroll = Payroll::where('payroll_number', $payroll_number)
                        ->first()->toArray();

            //explode period
            $date = explode(' - ', $payroll['period']);

            $payroll['period_from'] = $date[0];
            $payroll['period_to'] = $date[1];
            
            //allowances
            $payroll['allowances'] = json_decode($payroll['allowances'],true);
            $payroll['days'] = json_decode($payroll['days'],true);
            $payroll['overtimes'] = json_decode($payroll['overtimes'],true);
            $payroll['sss'] = json_decode($payroll['sss'],true);
            $payroll['philhealth'] = json_decode($payroll['philhealth'],true);
            $payroll['pagibig'] = json_decode($payroll['pagibig'],true);
            $payroll['tax'] = json_decode($payroll['tax'],true);

            $payroll['exists'] = 1;
                        
            $employee = Employee::where('user_id', $payroll['employee_id'])->first();

            $employees = Employee::all();

            return view('payroll_ledger.preview', compact('employee', 'payroll', 'employees'));
        }
    }
}
