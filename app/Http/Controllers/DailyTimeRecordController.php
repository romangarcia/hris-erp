<?php

namespace App\Http\Controllers;

use App\Attendance;
use Illuminate\Http\Request;
use App\LeaveRequest;
use App\Employee;
use DataTables;
use Excel;

class DailyTimeRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function history(){
        $employees = Employee::orderby('first_name', 'ASC')->orderby('last_name', 'ASC')->get();
        return view('daily_time_record.history',['employees'=>$employees]);
    }

    public function get_history(Request $request){
        $select_qry = array(
            'employees.first_name',
            'employees.last_name',
            'attendances.id as r_id',
            'attendances.time_in',
            'attendances.time_out',
            'attendances.total',
            'attendances.night_shift',
            'attendances.updated_at',
            'attendances.date',
        );
        
        $currentMonth = date("m");
        $currentYear = date("Y");

        $employee_id = $request->employee_id;
        $date = $request->date;
        $filters=[];
        if($employee_id != ''){
            $filters['employee_id'] = $employee_id;
        }

            if($date!=''){
                $date_array  = explode(" - ",$date);
                $from = date('Y-m-d',strtotime($date_array[0]));
                $to = date('Y-m-d',strtotime($date_array[1]));
                $data = Attendance::where($filters)
                    ->whereBetween('attendances.date',[$from,$to])
                    ->join('employees', 'employees.id',  '=', 'attendances.employee_id')
                    ->get($select_qry);
            } else {
                $data = Attendance::where($filters)
                    ->whereRaw('MONTH(attendances.date) = ?', [$currentMonth])
                    ->whereRaw('YEAR(attendances.date) = ?', [$currentYear])
                    ->join('employees', 'employees.id',  '=', 'attendances.employee_id')
                    ->get($select_qry);
            }


        $data_tables = Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('date', function($row){

                $response = $row->date;
                if($row->night_shift == 1){
                    $response .= '<small><span class="badge badge-dark">night</span></small';
                }

                return $response;
            })
            ->addIndexColumn()
            ->addColumn('name', function($row){
                return ucwords($row->first_name).' '.ucwords($row->last_name);
            })
			->addIndexColumn()
            ->addColumn('time_in', function($row){
                return date('H:i', strtotime($row->time_in));
            })
			->addIndexColumn()
            ->addColumn('time_out', function($row){
                return date('H:i', strtotime($row->time_out));
            })
            ->addIndexColumn()
            ->addColumn('total', function($row){
                $response = '00:00:00';

                if($row->time_out != '-'){
                    $total_time = 60 * $row->total;
                    $response = gmdate("H:i:s", $total_time);
                }

                return $response;
            })
			->addIndexColumn()
            ->addColumn('last_updated', function($row){
                $response = '';
                if($row->updated_at != ''){
                    $response = date("Y-m-d", strtotime($row->updated_at));
                }
                return $response;
            })
            ->addIndexColumn()
            ->rawColumns(['date','name','time_in','time_out','total', 'last_updated'])
            ->make(true);
        
        return $data_tables;

    }
    
    public function filter(){
        $currentMonth = date("m");
        $currentYear = date("Y");
        $records = Attendance::whereRaw('MONTH(date) = ?', [$currentMonth])
        ->whereRaw('YEAR(attendances.date) = ?', [$currentYear])
        ->paginate(20);


        $employees = Employee::orderby('first_name', 'ASC')->orderby('last_name', 'ASC')->get();

        return view('daily_time_record.filter',
            [
                'record'        =>  $records, 
                'employees'     =>  $employees
            ]
        );
    }

    public function filter_list(Request $request){
        //FILTER LIST VALIDATE
        // dd($request);
        
        $employee_id = $request->employee_id;
        $date_range = $request->date_range_picker;

        $filters = [];

        if($employee_id!=''){
            $filters['employee_id'] = $employee_id;
        }

        $to_select = array(
            'attendances.*',
            'employees.employee_number as employee_no',
        ); 

        if($date_range != ''){
			$date_array  = explode(" - ",$date_range);
			$from = date('Y-m-d',strtotime($date_array[0]));
            $to = date('Y-m-d',strtotime($date_array[1]));

            $data = Attendance::where($filters)
            ->whereBetween('attendances.date',[$from,$to])
            ->join('employees', 'employees.id', '=', 'attendances.employee_id')
            ->get($to_select);
        }else{
            $data = Attendance::where($filters)
            ->join('employees', 'employees.id', '=', 'attendances.employee_id')
            ->latest()
            ->get($to_select);
        }
        
        $data_tables = Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('date', function($row){
                $return_date = $row->date;
                
                if($row->night_shift == true){
                    $return_date .= ' <small><span class="badge badge-dark">night</span></small';
                }

                return $return_date;
            })
            ->addIndexColumn()
            ->addColumn('employee_id', function($row){
                return $row->employee_no;
            })
            ->addIndexColumn()
            ->addColumn('employee_name', function($row){
                return $row->name;
            })
            ->addIndexColumn()
            ->addColumn('time_in', function($row){
                return date('H:i', strtotime($row->time_in));
            })
            ->addIndexColumn()
            ->addColumn('time_out', function($row){
                return date('H:i', strtotime($row->time_out));
            })
            ->addIndexColumn()
            ->addColumn('total', function($row){
                $time_in = strtotime($row->time_in);
                $time_out = strtotime($row->time_out);
                $total_time = $time_out - $time_in;
                return gmdate("H:i:s", $total_time-3600);
            })
            ->rawColumns(['date','employee_id','employee_name','time_in','time_out', 'total'])
            ->make(true);
            
        return $data_tables;
    }

    public function tardiness(){
        $records = Attendance::where('total', '<', 8)
        ->paginate(20);
		$employees = Employee::orderby('first_name', 'ASC')->orderby('last_name', 'ASC')->get();
        return view('daily_time_record.tardiness',['record'=>$records,'employees'=>$employees]);
    }
	public function tardiness_list(Request $request){
		$employee_id = $request->employee_id;
        $date_range = $request->date_range_picker;

        $filters = [];

        if($employee_id!=''){
            $filters['employee_id'] = $employee_id;
        }

        $to_select = array(
            'attendances.*',
            'employees.employee_number as employee_no',
        ); 

        if($date_range != ''){
			$date_array  = explode(" - ",$date_range);
			$from = date('Y-m-d',strtotime($date_array[0]));
            $to = date('Y-m-d',strtotime($date_array[1]));

            $data = Attendance::where($filters)->where('total', '<', 8)
            ->whereBetween('attendances.date',[$from,$to])
            ->join('employees', 'employees.id', '=', 'attendances.employee_id')
            ->get($to_select);
        }else{
            $data = Attendance::where($filters)->where('total', '<', 8)
            ->join('employees', 'employees.id', '=', 'attendances.employee_id')
            ->latest()
            ->get($to_select);
        }
        
        $data_tables = Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('date', function($row){
                $return_date = $row->date;
                
                if($row->night_shift == true){
                    $return_date .= ' <small><span class="badge badge-dark">night</span></small';
                }

                return $return_date;
            })
            ->addIndexColumn()
            ->addColumn('employee_name', function($row){
                return $row->name;
            })
            ->addIndexColumn()
            ->addColumn('time_in', function($row){
				$time_in = date('H:i', strtotime($row->time_in));
                $time_in .= ' <abbr title="'.date('h:i a @ D M-j-Y', strtotime($row->time_in)).'"><small><em>'.date('Y-m-d', strtotime($row->time_in)).'</em></small></abbr>';
                return $time_in;
            })
            ->addIndexColumn()
            ->addColumn('time_out', function($row){
				$time_out = '';
				 if(strlen($row->time_out)!=1){
                       $time_out= date('H:i', strtotime($row->time_out)).'<abbr title="'.date('h:i a @ D M-j-Y', strtotime($row->time_out)).'"><small><em>'.date('Y-m-d', strtotime($row->time_out)).'</em></small></abbr>';
				 }else{
                       $time_out =  '--:--';
				 }
					
                return $time_out;
            })
            ->addIndexColumn()
            ->addColumn('total', function($row){
                $time_in = strtotime($row->time_in);
                $time_out = strtotime($row->time_out);
                $total_time = $time_out - $time_in;
                return gmdate("H:i:s", $total_time-3600);
            })
            ->rawColumns(['date','employee_name','time_in','time_out', 'total'])
            ->make(true);
            
        return $data_tables;
	}
    public function absences(){
        
        $records = LeaveRequest::where('state_status', '=', 'Approved')
        ->paginate(5);

        $records = LeaveRequest::join('users', 'users.id', '=', 'leave_requests.user_id')
        ->where('leave_requests.state_status', '=', 'Approved')
        ->paginate(5, 
            array(
                'leave_requests.*',
                'leave_requests.created_at as date_request',
                'users.name as name',
            )
        );

        $employees = Employee::orderby('first_name', 'ASC')->orderby('last_name', 'ASC')->get();
        return view('daily_time_record.absences',['record'=>$records,'employees'=>$employees]);
    }
	public function absence_list(Request $request)
	{
		$employee_id = $request->search_employee_id;
        $date_range = $request->date_range_picker;
        $datefiled = $request->datefiled;
        $approveby = $request->approveby;
        $filters = [];

        if($employee_id!='') {
            $filters['employee_id'] = $employee_id;
        }
        if($datefiled!='') {
            $filters['date_filed'] = $datefiled;
        }
        if($approveby != '') {
            $filters['approved_by'] = $approveby;
        }

        $to_select = array(
            'leave_requests.*',
            'leave_requests.created_at as date_request',
            'users.name as name',
        ); 

        if($date_range != ''){
			$date_array  = explode(" - ",$date_range);
			$from = date('Y-m-d',strtotime($date_array[0]));
            $to = date('Y-m-d',strtotime($date_array[1]));

            $data = LeaveRequest::join('users', 'users.id', '=', 'leave_requests.user_id')
        	->where('leave_requests.state_status', '=', 'Approved')
            ->whereBetween('leave_requests.date_filed',[$from,$to])
            ->orWhereBetween('leave_requests.date_start',[$from,$to])
            ->orWhereBetween('leave_requests.date_end',[$from,$to])
            ->orWhereBetween('leave_requests.approved_date',[$from,$to])
            ->get($to_select);
        }else{
            $data = LeaveRequest::where($filters)
            ->join('users', 'users.id', '=', 'leave_requests.user_id')
        	->where('leave_requests.state_status', '=', 'Approved')
            ->latest()
            ->get($to_select);
        }
		$data_tables = Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('name', function($row){
             
                return $row->name;
            })
            ->addIndexColumn()
            ->addColumn('date_filed', function($row){
                return $row->date_filed;
            })
			->addIndexColumn()
            ->addColumn('date_start', function($row){
                return $row->date_start;
            })
			->addIndexColumn()
            ->addColumn('date_end', function($row){
                return $row->date_end;
            })
            ->addIndexColumn()
            ->addColumn('type', function($row){
                return $row->type;
            })
			
			->addIndexColumn()
            ->addColumn('approved_by', function($row){
                return $row->approved_by;
            })
			->addIndexColumn()
            ->addColumn('approved_date', function($row){
                return $row->approved_date;
            })
			
            ->rawColumns(['name','date_filed','date_start','date_end','type','approved_by','approved_date'])
            ->make(true);
            
        return $data_tables;
	}
	function excel_tardiness(Request $request)
    {
		
		$employee_id = $request->search_employee_id;
        $date_range = $request->date_range_picker;

         $filters = [];

        if($employee_id!=''){
            $filters['employee_id'] = $employee_id;
        }

        $to_select = array(
            'attendances.*',
            'employees.employee_number as employee_no',
        ); 

        if($date_range != ''){
			$date_array  = explode(" - ",$date_range);
			$from = date('Y-m-d',strtotime($date_array[0]));
            $to = date('Y-m-d',strtotime($date_array[1]));

            $attendence_data = Attendance::where($filters)->where('total', '<', 8)
            ->whereBetween('attendances.date',[$from,$to])
            ->join('employees', 'employees.id', '=', 'attendances.employee_id')
            ->get($to_select)->toArray();
        }else{
            $attendence_data = Attendance::where($filters)->where('total', '<', 8)
            ->join('employees', 'employees.id', '=', 'attendances.employee_id')
            ->latest()
            ->get($to_select)->toArray();
        }
		
		 //$attendence_data = Attendance::where('total', '<', 8)->get()->toArray();
		 $attendence_array[] = array('Date', 'Name', 'Time-In', 'Time-Out', 'Total');
		 foreach($attendence_data as $attendence)
		 {
		  $attendence_array[] = array(
		   'Date'  => ($attendence['night_shift'] == true)?$attendence['date'].' ( night ) ':$attendence['date'],
		   'Name'   => $attendence['name'],
		   'Time-In'    => date('H:i', strtotime($attendence['time_in'])).'('.date('Y-m-d', strtotime($attendence['time_in'])).')',
		   'Time-Out'  => (strlen($attendence['time_out'])!=1)?date('H:i', strtotime($attendence['time_out'])).'('.date('Y-m-d', strtotime($attendence['time_out'])).')':"--:--",
		   'Total'   => gmdate("H:i:s",((strtotime($attendence['time_out']) - strtotime($attendence['time_in']))-3600))
		  );
		 }
		 Excel::create('Tardiness_Report_'.date('Ymd').'_'.date('His'), function($excel) use ($attendence_array){
		  $excel->setTitle('Tardiness Data');
		  $excel->sheet('Tardiness Data', function($sheet) use ($attendence_array){
		   $sheet->fromArray($attendence_array, null, 'A1', false, false);
		  });
    	 })->download('xlsx');
    }
	function download_filter_export(Request $request)
	{
		
		$employee_id = $request->employee_id;
        $date_range = $request->date_range_picker;

        $filters = [];

        if($employee_id!=''){
            $filters['employee_id'] = $employee_id;
        }

        $to_select = array(
            'attendances.*',
            'employees.employee_number as employee_no',
        ); 

        if($date_range != ''){
			$date_array  = explode(" - ",$date_range);
			$from = date('Y-m-d',strtotime($date_array[0]));
            $to = date('Y-m-d',strtotime($date_array[1]));

            $data = Attendance::where($filters)
            ->whereBetween('attendances.date',[$from,$to])
            ->join('employees', 'employees.id', '=', 'attendances.employee_id')
            ->get($to_select);
        }else{
            $data = Attendance::where($filters)
            ->join('employees', 'employees.id', '=', 'attendances.employee_id')
            ->latest()
            ->get($to_select);
        }
        
			$filter_array = [];
			foreach($data as $d)
			{
				  $time_in = strtotime($d->time_in);
					$time_out = strtotime($d->time_out);
					$total_time = $time_out - $time_in;

				$filter_array[]=array(
					'Date'=>$d->date,
					'Employee No'=>$d->employee_no,
					'Name'=>$d->name,
					'Time-In'=>date('H:i', strtotime($d->time_in)),
					'Time-Out'=>date('H:i', strtotime($d->time_out)),
					'Total'=>gmdate("H:i:s", $total_time-3600)
				);
			}
			Excel::create('DTR_filter_reports', function($excel) use ($filter_array) {
				$excel->sheet('DTR_Sheet', function($sheet) use ($filter_array)
				{

				  $sheet->fromArray($filter_array);

				});
		   })->download('xlsx');
		
	}	
	function download_absence_excel(Request $request)
	{
		$employee_id = $request->search_employee_id;
        $date_range = $request->date_range_picker;

        $filters = [];
		
		 if($employee_id!=''){
            $filters['employee_id'] = $employee_id;
        }

        $to_select = array(
            'leave_requests.*',
            'leave_requests.created_at as date_request',
            'users.name as name',
        ); 

        if($date_range != ''){
			$date_array  = explode(" - ",$date_range);
			$from = date('Y-m-d',strtotime($date_array[0]));
            $to = date('Y-m-d',strtotime($date_array[1]));

            $data = LeaveRequest::join('users', 'users.id', '=', 'leave_requests.user_id')
        	->where('leave_requests.state_status', '=', 'Approved')
            ->whereBetween('leave_requests.date_filed',[$from,$to])
            ->orWhereBetween('leave_requests.date_start',[$from,$to])
            ->orWhereBetween('leave_requests.date_end',[$from,$to])
            ->orWhereBetween('leave_requests.approved_date',[$from,$to])
            ->get($to_select);
        }else{
            $data = LeaveRequest::join('users', 'users.id', '=', 'leave_requests.user_id')
        	->where('leave_requests.state_status', '=', 'Approved')
            ->latest()
            ->get($to_select);
        }
		$absence_array = [];
		foreach($data as $d)
		{

			$absence_array[]=array(
				'Name'=>$d->name,
				'Date Filed'=>$d->date_filed,
				'Date Start'=>$d->date_start,
				'Date End'=>$d->date_end,
				'Type'=>$d->type,
				'Reason'=>$d->reason,
				'File Path'=>$d->filepath,
				'Approved By'=>$d->approved_by,
				'Approved Date'=>$d->approved_date,
				'Remarks'=>$d->remarks
			);
		}
		Excel::create('DTR_absense_report_'.date('Ymd').'_'.date('His'), function($excel) use ($absence_array) {
			$excel->sheet('DTR_absence_Sheet', function($sheet) use ($absence_array)
			{

			  $sheet->fromArray($absence_array);

			});
	   })->download('xlsx');
    }	
    
    function download_history_report(Request $request){
        $select_qry = array(
            'employees.first_name',
            'employees.last_name',
            'attendances.id as r_id',
            'attendances.time_in',
            'attendances.time_out',
            'attendances.total',
            'attendances.night_shift',
            'attendances.updated_at',
            'attendances.date',
        );
        
        $currentMonth = date("m");
        $currentYear = date("Y");

        $employee_id = $request->employee_id;
        $date = $request->date;
        $filters=[];
        if($employee_id != ''){
            $filters['employee_id'] = $employee_id;
        }

        if($date!=''){
            $date_array  = explode(" - ",$date);
            $from = date('Y-m-d',strtotime($date_array[0]));
            $to = date('Y-m-d',strtotime($date_array[1]));
            $data = Attendance::where($filters)
                ->whereBetween('attendances.date',[$from,$to])
                ->join('employees', 'employees.id',  '=', 'attendances.employee_id')
                ->get($select_qry);
        } else {
            $data = Attendance::where($filters)
                ->whereRaw('MONTH(attendances.date) = ?', [$currentMonth])
                ->whereRaw('YEAR(attendances.date) = ?', [$currentYear])
                ->join('employees', 'employees.id',  '=', 'attendances.employee_id')
                ->get($select_qry);
        }

        $data_array = [];
        foreach($data as $row){
            $total = '00:00:00';

            if($row->time_out != '-'){
                $total_time = 60 * $row->total;
                $total = gmdate("H:i:s", $total_time);
            }

            $updated_at = '';
            if($row->updated_at != ''){
                $updated_at = date("Y-m-d", strtotime($row->updated_at));
            }

            $date_type = $row->date;
            if($row->night_shift == true){
                $date_type .= ' (Night)';
            }
            
			$data_array[] = array(
				'Date'          => $date_type,
				'Name'          => ucwords($row->first_name).' '.ucwords($row->last_name),
				'Time In'       => date('H:i', strtotime($row->time_in)),
				'Time Out'      => date('H:i', strtotime($row->time_out)),
				'Total'         => $total,
				'Last Updated'  => $updated_at,
			);
        }
        
        Excel::create('History-Report-'.date("Ymd-his"), function($excel) use ($data_array) {
			$excel->sheet('HistoryReport-'.date("Ymd-his"), function($sheet) use ($data_array){
			  $sheet->fromArray($data_array);
			});
	   })->download('xlsx');

    }
}