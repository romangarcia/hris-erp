<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Department;

use DataTables;
use Validator;
class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = Role::get();
        $roles = getRouteNames();
        $methods = getDefaultMethods();
        $departments = Department::orderBy('name', 'ASC')->get();

        return view('roles.index',compact('data', 'roles', 'methods','departments'));
    }

    public function role_list(Request $request)
    {
        
        // $role_access      = $request->role_access;
        $department_id  = $request->department_id;
        // $is_active      = $request->is_active;
        // $role_name  = $request->role_name;

        // $filters = [];

        // if($role_access != ''){
        //     $filters['roles.role'] = $role_access;
        // }
        // if($department_id != ''){
        //     $filters['roles.department_id'] = $department_id;
        // }
        // if($role_name != ''){
        //     $filters['roles.role_name'] = $role_name;
        // }
        // if($is_active != ''){
        //     $filters['roles.is_active'] = $is_active;
        // }

        // $to_select = array(
        //     'roles.role',
        //     'roles.page',
        //     'roles.id as r_id',
        //     'roles.is_active',
        //     'roles.department_id',
        //     'roles.role_name',
        //     'departments.name as department_name'
        // );

        // if(count($filters) > 0){
        //     $data = Role::join('departments', 'roles.department_id', '=', 'departments.id')
        // 	->where($filters)
        //     ->get($to_select);
        // }else{
        //     $data = Role::join('departments', 'roles.department_id', '=', 'departments.id')
        //     ->get($to_select);
        // }

        if($department_id != ''){
            $data = Department::where('id', $department_id)
            ->orderBy('name', 'desc')
            ->get(array('id as r_id', 'name as department_name'));
        }else{
            $data = Department::orderBy('name', 'desc')
            ->get(array('id as r_id', 'name as department_name'));
        }

        $data_tables = Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('id', function($row){
                $url_edit = route('role.edit_department', [$row->r_id]);
                $response = '<a href="'.$url_edit.'">'.$row->r_id.'</a>';
                return $response;
            })
            ->addIndexColumn()
            ->addColumn('department', function($row){
                return ucfirst($row->department_name);
            })
            // ->addIndexColumn()
            // ->addColumn('role_name', function($row){
            //     return ucfirst($row->role_name);
            // })
            // ->addIndexColumn()
            // ->addColumn('page', function($row){
            //     return ucfirst($row->page);
			// })
			// ->addIndexColumn()
            // ->addColumn('role', function($row){
            //     return ucfirst($row->role);
			// })
			// ->addIndexColumn()
            // ->addColumn('active', function($row){
				
			// 	if($row->is_active){
			// 		$status = '<div class="badge badge-success badge-pill">&nbsp;&nbsp;YES&nbsp;&nbsp;</div>';
			// 	}else{
			// 		$status = '<div class="badge badge-danger badge-pill">&nbsp;&nbsp;NO&nbsp;&nbsp;</div>';
			// 	}

            //     return $status;
			// })
			->addIndexColumn()
            ->addColumn('action', function($row){
				$url_edit = route('role.edit_department', [$row->r_id]);
				$response = '<button  type="button" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm" onclick="window.location.href = \''.$url_edit.'\';"><i style="margin-left: -6px;" class="mdi mdi-lead-pencil"></i></button>';
                // $response .= ' <button type="button" data-toggle="modal" data-id="'.$row->r_id.'" data-target="#DeleteModal" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm btn-delete_row"><i style="margin-left: -7px;" class="mdi mdi-delete"></i></button>';
                return $response;
			})
            // ->rawColumns(['id','role_name','page','role','active', 'action']) //department
            ->rawColumns(['id','department_name', 'action']) //department
            ->make(true);
            
        return $data_tables;
    }

    public function edit_department($id){
        $data = Role::get();
        $pages = getRouteNames();
        $methods = getDefaultMethods();
        $department = Department::findOrFail($id);

        return view('roles.department_edit',compact('data', 'pages', 'methods', 'department'));
    }

    public function edit_department_filter(Request $request){
        $role_access      = $request->role_access;
        $department_id  = $request->department_id;
        $is_active      = $request->is_active;
        $page      = $request->page;

        $filters = [];
        if($department_id != ''){
            $filters['roles.department_id'] = $department_id;
        }
        if($role_access != ''){
            $filters['roles.role'] = $role_access;
        }
        if($is_active != ''){
            $filters['roles.is_active'] = $is_active;
        }
        if($page != ''){
            $filters['roles.page'] = $page;
        }

        $to_select = array(
            'roles.role',
            'roles.page',
            'roles.id as r_id',
            'roles.is_active',
            'roles.department_id',
            'roles.role_name',
            'departments.name as department_name'
        );

        if(count($filters) > 0){
            $data = Role::join('departments', 'roles.department_id', '=', 'departments.id')
        	->where($filters)
            ->get($to_select);
        }else{
            $data = Role::join('departments', 'roles.department_id', '=', 'departments.id')
            ->get($to_select);
        }

        $data_tables = Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('id', function($row){
                $url_edit = route('roles.edit', [$row->r_id]);
                $response = '<a href="'.$url_edit.'">'.$row->r_id.'</a>';
                return $response;
            })
            ->addIndexColumn()
            ->addColumn('page', function($row){
                return ucfirst($row->page);
            })
			->addIndexColumn()
            ->addColumn('role', function($row){
                return ucfirst($row->role);
			})
			->addIndexColumn()
            ->addColumn('is_active', function($row){
				if($row->is_active){
					$status = '<div class="badge badge-success badge-pill">&nbsp;&nbsp;YES&nbsp;&nbsp;</div>';
				}else{
					$status = '<div class="badge badge-danger badge-pill">&nbsp;&nbsp;NO&nbsp;&nbsp;</div>';
				}
                return $status;
			})
			->addIndexColumn()
            ->addColumn('action', function($row){
				$url_edit = route('roles.edit', [$row->r_id]);
				$response = '<button  type="button" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm" onclick="window.location.href = \''.$url_edit.'\';"><i style="margin-left: -6px;" class="mdi mdi-lead-pencil"></i></button>';
                $response .= ' <button type="button" data-toggle="modal" data-id="'.$row->r_id.'" data-target="#DeleteModal" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm btn-delete_row"><i style="margin-left: -7px;" class="mdi mdi-delete"></i></button>';
                return $response;
			})
            ->rawColumns(['id', 'page','role', 'is_active', 'action']) //department
            ->make(true);
            
        return $data_tables;
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        $pages = getRouteNames();
        $methods = getDefaultMethods();
        return view('roles.create', compact('departments', 'pages', 'methods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $is_validated = Validator::make($request->all(), [
            'department_id'     =>  'required',
            'page'              =>  'required',
            'role'              =>  'required',
            'is_active'         =>  'required',
        ]);
        
        if($is_validated->fails()){
			return back()->withErrors($is_validated)->withInput();
		}

        $role = new Role;
        $role->department_id = $request->department_id;
        $role->page = $request->page;
        $role->role = $request->role;
        $role->is_active = $request->is_active;
        $role->save();
        return redirect()->back()->with('success', 'Successfully created'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $departments = Department::all();
        $pages = getRouteNames();
        $methods = getDefaultMethods();
        return view('roles.edit', compact('role', 'departments', 'pages', 'methods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $is_validated = Validator::make($request->all(), [
            'department_id'     =>  'required',
            'page'              =>  'required',
            'role'              =>  'required',
            'is_active'         =>  'required',
        ]);
        
        if($is_validated->fails()){
			return back()->withErrors($is_validated)->withInput();
        }
        
        $role = Role::findOrFail($id);

        $role->department_id = $request->department_id;
        $role->page = $request->page;
        $role->role = $request->role;
        $role->is_active = $request->is_active;
        $role->role_name = $request->role_name;
        $role->save();

        return redirect('roles')->with('success', 'Successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect('roles')->with('success', 'Successfully deleted');
    }

    // public function enable()
    // {
    //     return "controller";
   
    //     return response()->json([
    //         'error' => false,
    //         'task'  => $task,
    //     ], 200);

        // $user = Role::findOrFail($request->user_id);
        // if($user->active == 1){
        //     $user->active = 0;
        // } else {
        //     $user->active = 1;
        // }

    // }

    private function splitWords($word){
        $nc = str_replace('Controller', '', $word);
        $pattern = '/(.*?[a-z]{1})([A-Z]{1}.*?)/';
        $replace = '${1} ${2}';
        $word = strtoupper(preg_replace($pattern, $replace, $nc));
        return $word;
    }
}
