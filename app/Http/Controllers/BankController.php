<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;
use Session;
use DataTables;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks = Bank::orderBy('bank_name')->paginate(10);

        return view('bank.index', compact('banks'));
            // ->with([
            //     'banknum' => Session::get("bankid"),
            //     'i' => (request()->input('page', 1) - 1) * 5
            // ]);
    }

    public function bank_list(Request $request)
    {
        $bank_id = $request->bank_id;
        
		$filters = [];
        if($bank_id!=''){
         	$filters['id'] = $bank_id;
        }
		$data = Bank::where($filters)->latest()->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('bank_name', function($row){
                    return ($row->bank_name)??"";
                })
                ->addIndexColumn()
                ->addColumn('IBAN', function($row){
                    return ($row->IBAN)??"";
                })
                ->addIndexColumn()
                ->addColumn('BIC', function($row){
                    return ($row->BIC)??"";
                })
                ->addIndexColumn()
                ->addColumn('member_no', function($row){
                    return ($row->member_no)??"";
                })
                ->addIndexColumn()
                ->addColumn('clearing_no', function($row){
                    return ($row->clearning_no)??"";
                })
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="'.url('bank/' . $row->id . '/edit').'"><i class="mdi mdi-lead-pencil"></i></a>';
                    $btn .= '<a href="javascript:;" data-toggle="modal" onclick="deleteData('.$row->id.')" data-target="#DeleteModal"><i class="mdi mdi-delete"></i></a>';
                    return $btn;
                })

                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $url_edit = route('bank.edit', [$row->id]);
                    $btn = '<button type="button" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm" onclick="window.location.href = \''.$url_edit.'\';"><i style="margin-left: -6px;" class="mdi mdi-lead-pencil"></i></button>';
                    $btn .= '<button type="button" data-toggle="modal" data-id="'.$row->id.'" data-target="#DeleteModal" onclick="deleteData('.$row->id.')" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm btn-delete_row"><i style="margin-left: -7px;" class="mdi mdi-delete"></i></button>';
                    return $btn;
                })
                ->rawColumns(['bank_name','IBAN','BIC','member_no','clearing_no','action'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bank.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bank = new Bank;
        $bank->bank_name = $request->bank_name;
        $bank->address = $request->address;
        $bank->extra_address = $request->extra_address;
        $bank->city = $request->city;
        $bank->state = $request->state;
        $bank->zipcode = $request->zipcode;
        $bank->country = $request->country;
        $bank->iban = $request->iban;
        $bank->bic = $request->bic;
        $bank->member_no = $request->member_no;
        $bank->clearing_no = $request->clearing_no;
        $bank->save();

        $banks = Bank::orderBy('bank_name', 'asc')->get();
        return view('bank.index', compact('banks'))->with('success', 'Bank record successfully added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank = Bank::findOrFail($id);
        return view('bank.edit', compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bank = Bank::findOrFail($id);
        $bank->bank_name = $request->bank_name;
        $bank->address = $request->address;
        $bank->extra_address = $request->extra_address;
        $bank->city = $request->city;
        $bank->state = $request->state;
        $bank->zipcode = $request->zipcode;
        $bank->country = $request->country;
        $bank->iban = $request->iban;
        $bank->bic = $request->bic;
        $bank->member_no = $request->member_no;
        $bank->clearing_no = $request->clearing_no;
        $bank->save();

        $banks = Bank::orderBy('bank_name')->get();
        return view('bank.index', compact('banks'))->with('success', 'Record successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = Bank::findOrFail($id);
        $bank->delete();

        $banks = Bank::orderBy('bank_name', 'asc')->get();
        return view('bank.index', compact('banks'))->with('success', 'Record deleted!');
    }

    // public function bankSearch(Request $request)
    // {
    //     $bank_id = $request->bank_id;
        
	// 	$filters = [];
    //     if($bank_id!=''){
    //      	$filters['id'] = $bank_id;
    //     }
	// 	$data = Bank::where($filters)->latest()->get();
    //     return Datatables::of($data)
    //             ->addIndexColumn()
    //             ->addColumn('bank_name', function($row){
    //                 return ($row->bank_name)??"";
    //             })
    //             ->addIndexColumn()
    //             ->addColumn('IBAN', function($row){
    //                 return ($row->IBAN)??"";
    //             })
    //             ->addIndexColumn()
    //             ->addColumn('BIC', function($row){
    //                 return ($row->BIC)??"";
    //             })
    //             ->addIndexColumn()
    //             ->addColumn('member_no', function($row){
    //                 return ($row->member_no)??"";
    //             })
    //             ->addIndexColumn()
    //             ->addColumn('clearing_no', function($row){
    //                 return ($row->clearning_no)??"";
    //             })
    //             ->addIndexColumn()
    //             ->addColumn('action', function($row){
    //                 // $btn = '<a href="'.url('bank/' . $row->id . '/edit').'"><i class="mdi mdi-lead-pencil"></i></a>';
    //                 // $btn .= '<a href="javascript:;" data-toggle="modal" onclick="deleteData('.$row->id.')" data-target="#DeleteModal"><i class="mdi mdi-delete"></i></a>';


    //                 $url_edit = route('roles.edit', [$row->id]);
    //                 $btn = '<button type="button" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm" onclick="window.location.href = \''.$url_edit.'\';"><i style="margin-left: -6px;" class="mdi mdi-lead-pencil"></i></button>';
    //                 $btn .= '<button type="button" data-toggle="modal" data-id="'.$row->id.'" data-target="#DeleteModal" class="btn btn-outline-secondary btn-rounded btn-icon btn-sm btn-delete_row"><i style="margin-left: -7px;" class="mdi mdi-delete"></i></button>';
    //                 return $btn;
    //             })
    //             ->rawColumns(['bank_name','IBAN','BIC','member_no','clearing_no','action'])
    //             ->make(true);
                    
    // }
}
